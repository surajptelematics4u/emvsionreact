import React from "react";
import { FluidNavigator } from 'react-navigation-fluid-transitions';
import { createStackNavigator, createDrawerNavigator } from "react-navigation";
import { Root } from 'native-base';

import DrawerHeader from "./component/DrawerHeader";

import LoginScreen from "./screen/loginscreen"; 
import SplashScreen from "./screen/splashscreen";
import AlertListScreen from "./screen/alertlist";
import AlertDetailsScreen from "./screen/alertdetails";
import LiveLocationScreen from "./screen/livelocationscreen";
import TripDetailsScreen from "./screen/tripdetails";
import TripListScreen from "./screen/triplist";
import TripSearchScreen from "./screen/tripsearch";
import LiveVisionSearchScreen from "./screen/livevisionsearch";
import OnTripMapScreen from "./screen/ontripmap";

import SLADashboardScreen from "./screen/sladashboard";
import LiveVisionScreen from "./screen/livevision";
import NoAvailableScreen from "./screen/noauth";

const Drawer = createDrawerNavigator({ 
  SLADashboard: {
    screen: SLADashboardScreen
  },
  LiveVision: {
    screen: LiveVisionScreen
  },
  NoAvailable: {
    screen: NoAvailableScreen
  }
},
{
 initialRouteName: 'SLADashboard',
  drawerPosition: 'left',
  
  contentComponent: DrawerHeader,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle'
});

const LoginNavigator = FluidNavigator(
  {
      LoginSub: {  screen: LoginScreen  }
  },
  {
    initialRouteName: "LoginSub",
    headerMode: "none"
  }
);

const AppNavigator = createStackNavigator(
    {
        Splash: {  screen: SplashScreen  },
        Login: {  screen: LoginScreen  }, 
        Drawer: {  screen: Drawer },
        LiveLocation : {  screen: LiveLocationScreen  },
        AlertList :  {  screen: AlertListScreen  },
        AlertDetails :  {  screen: AlertDetailsScreen  },
        TripDetails:  {  screen: TripDetailsScreen  },
        TripList:  {  screen: TripListScreen  },
        TripSearch:  {  screen: TripSearchScreen  },
        LiveVisionSearch: {  screen: LiveVisionSearchScreen  },
        OnTripMap:  {  screen: OnTripMapScreen  }
    },
    {
      //index: 0,
      initialRouteName: "Splash",
      headerMode: "none",
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  );

  export default () =>
    <Root>
      <AppNavigator />
    </Root>;


// this.props.navigation.navigate('NestedNavigator1', {}, NavigationActions.navigate({ routeName: 'screenB' }))