import {
  BASEURL,domainName,BASEURLG
} from '../util/Constant'; 
import { AsyncStorage } from "react-native";
 
import fetchData from '../component/CancelableFetch';

export const getdomainName = () => {
  return AsyncStorage.getItem(domainName)
         .then((result) => result );
}


export const tripAlertsAPI = ({loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'TripAlerts?SystemId='+loginData.systemId+'&CustomerId='+loginData.customerid);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG  + 'TripAlerts?SystemId='+loginData.systemId+'&CustomerId='+loginData.customerid, {
    method: 'GET'
   },4)
   .then((response) => response.json());
}

export const alertsDetailsAPI = ({alertId,PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG +  + 'AlertDetails?AlertId='+alertId+'&systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&PageSize=20&PageNo='+PageNo);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'AlertDetails?AlertId='+alertId+'&systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&PageSize=20&PageNo='+PageNo, {
    method: 'GET'
   },6)
   .then((response) => response.json());
}

export const getMoreInfoOnTrip = ({tripStatus,PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG +  'getMoreInfoOnTrip'+ " "+JSON.stringify({systemId: loginData.systemId, customerId: loginData.customerid,tripStatus: tripStatus}));
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'getMoreInfoOnTrip', {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({systemId: loginData.systemId, customerId: loginData.customerid,tripStatus: tripStatus})
   },7)
   .then((response) => response.json());
}

export const closeTripAPI = ({NoofDays,PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'TripInformation?SystemId='+loginData.systemId+'&CustomerId='+loginData.customerid+'&DataStatus='+NoofDays+'&PageSize=20&PageNo='+PageNo);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'TripInformation?SystemId='+loginData.systemId+'&CustomerId='+loginData.customerid+'&DataStatus='+NoofDays+'&PageSize=20&PageNo='+PageNo, {
    method: 'GET'
   },5)
   .then((response) => response.json());
}
//trip details
export const tripPathFromApi = ({TripId,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'TripLocation?TripId='+TripId);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'TripLocation?TripId='+TripId, {
    method: 'GET'
   },8)
   .then((response) => response.json());
}
//+'customer'
export const tripHistoryPathFromApi = ({otherParam,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'HistoryData?SystemId='+loginData.systemId+'&CustomerId='+loginData.customerid+'&VehicleNo='+otherParam.VehicleNo+'&StartDate='+otherParam.StartDate+'&EndDate='+otherParam.EndDate);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'HistoryData?SystemId='+loginData.systemId+'&CustomerId='+loginData.customerid+'&VehicleNo='+otherParam.VehicleNo+'&StartDate='+otherParam.StartDate+'&EndDate='+otherParam.EndDate, {
    method: 'GET'
   },9)
   .then((response) => response.json());
}

export const tripAlertPointsFromApi = ({TripId,AlertId,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'AlertPoints?TripId='+TripId+'&AlertId='+AlertId);
  return fetch('http://'+ loginData.domainName + ':7080' + BASEURLG + 'AlertPoints?TripId='+TripId+'&AlertId='+AlertId, {
    method: 'GET'
   },10)
   .then((response) => response.json());
}

export const openTripMAPAPI = ({PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'getDetailsForMapCluster?systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&PageSize=100&PageNumber='+PageNo);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'getDetailsForMapCluster?systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&PageSize=100&PageNumber='+PageNo, {
    method: 'GET'
   },11)
   .then((response) => response.json());
}

//liveVision
export const liveVisionTripAPI = ({PageNo,vehicleStatus,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'liveVisionFeedAndroid?UserId='+loginData.userId+'&Status='+vehicleStatus+'&PageSize=20&PageNo='+PageNo);
  return fetch('http://'+ loginData.domainName + ':7080' + BASEURLG + 'liveVisionFeedAndroidNew?UserId='+loginData.userId+'&Status='+vehicleStatus+'&PageSize=20&PageNo='+PageNo, {
    method: 'GET'
   })
   .then((response) => response.json());
}


export const currentLocationDataAPI = ({VehicleNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'liveLocation?UserId='+loginData.userId+'&VehicleNo='+VehicleNo );
  return fetch('http://'+ loginData.domainName + ':7080' + BASEURLG + 'vehicleliveLocation?UserId='+loginData.userId+'&VehicleNo='+VehicleNo , {
    method: 'GET'
   })
   .then((response) => response.json());
}

export const last24hoursmapviewAPI = ({VehicleNo,Hours,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'last24hoursmapview?UserId='+loginData.userId+'&VehicleNo='+VehicleNo+'&Hours='+Hours );
  return fetch('http://'+ loginData.domainName + ':7080' + BASEURLG + 'last24hoursmapview?UserId='+loginData.userId+'&VehicleNo='+VehicleNo+'&Hours='+Hours , {
    method: 'GET'
   })
   .then((response) => response.json());
}

export const last24hourslistviewAPI = ({VehicleNo,Hours,PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'last24hours?UserId='+loginData.userId+'&VehicleNo='+VehicleNo+'&PageSize=20&PageNo='+PageNo+'&Hours='+Hours);
  return fetch('http://'+ loginData.domainName + ':7080' + BASEURLG + 'last24hoursAndroid?UserId='+loginData.userId+'&VehicleNo='+VehicleNo+'&PageSize=20&PageNo='+PageNo+'&Hours='+Hours , {
    method: 'GET'
   })
   .then((response) => response.json());
}
export const vehicleAlertsApi = ({alertId,Hours,loginData,VehicleNo}) => {
   console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'getAlertDetailsForVehicle?alertId='+alertId+'&Hours='+Hours+'&systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&UserId='+loginData.userIdAsInt+'&VehNo='+VehicleNo);
   return fetch('http://'+ loginData.domainName + ':7080' + BASEURLG + 'getAlertDetailsForVehicle?alertId='+alertId+'&Hours='+6+'&systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&UserId='+loginData.userIdAsInt+'&VehNo='+VehicleNo, {
     method: 'GET'
    })
    .then((response) => response.json());

   
}
export const tripSearchAPI =({searchText,PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'searchFeedTrip?systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&PageSize=10&PageNumber='+PageNo+'&searchText='+searchText);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'searchFeedTrip?systemId='+loginData.systemId+'&customerId='+loginData.customerid+'&PageSize=10&PageNumber='+PageNo+'&searchText='+searchText , {
    method: 'GET'
   },1)
   .then((response) => response.json());
}

export const liveVisionSearchAPI = ({searchText,PageNo,loginData}) => {
  console.log('http://'+ loginData.domainName + ':7080' + BASEURLG + 'searchFeed?UserId='+loginData.userId+'&PageSize=10&PageNumber='+PageNo+'&VehicleNo='+searchText);
  return fetchData('http://'+ loginData.domainName + ':7080' + BASEURLG + 'searchFeed?UserId='+loginData.userId+'&PageSize=10&PageNumber='+PageNo+'&VehicleNo='+searchText , {
    method: 'GET'
   },2)
   .then((response) => response.json());
}

// feath ID
// searchFeedTrip : 1
// searchFeed : 2
// loginUser : 3
// TripAlerts : 4
// TripInformation : 5
// AlertDetails : 6
// getMoreInfoOnTrip : 7
// TripLocation : 8
// HistoryData : 9
// AlertPoints : 10
// getDetailsForMapCluster : 11
// searchFeedTrip : 1
// searchFeedTrip : 1
// searchFeedTrip : 1