import React,{Component} from 'react';
import {  View,TouchableOpacity,ImageBackground } from 'react-native';
import { Icon,H1 } from 'native-base';

import colours from "../theme/color";

const headerback = require("../assets/image/headerback.png");

const CustomHeader = ({onPress,onRefreshPress,onSearchPress,getHeaderText,isbackVisible,isSerIconVisible,isRefIconVisible}) => {
  return (
    <ImageBackground source={headerback} style={styles.imageContainer}>
    <View style={{height: 60,flex : 1,flexDirection: 'row',justifyContent: 'space-between',alignItems: "flex-end"}}>
          <TouchableOpacity  onPress={onPress} style={{flexDirection: 'row',justifyContent: 'flex-start',alignItems: "center",flex : 1,marginBottom : 5,padding: 10}} >
            {isbackVisible ?  <Icon name='arrow-back' style={{ color: colours.white}} /> : <Icon name='md-list' style={{ color: colours.white}} />} 
            <H1 style = {styles.textStyle} > {getHeaderText} </H1>
          </TouchableOpacity>
        <View  style={{alignItems: "flex-end",justifyContent: 'center',flexDirection: 'row'}} >
          {isRefIconVisible &&  <TouchableOpacity onPress={onRefreshPress} style={{alignItems: "flex-end",justifyContent: 'center',marginBottom : 5,padding: 10}} >
              <Icon name='refresh' style={{color: colours.white}} />
              </TouchableOpacity>
          }
          {isSerIconVisible &&  <TouchableOpacity  onPress={onSearchPress} style={{alignItems: "flex-end",justifyContent: 'center',marginBottom : 5,padding: 10}} >
              <Icon name='search' style={{color: colours.white}} />
              </TouchableOpacity>
          }
        </View>
          
        </View>
    </ImageBackground>
  );
};

// const CustomHeader = ({onPress,getHeaderText,istextVisible,isBackIconVisible}) => {
//         return (
//           <View style={{height: 60, backgroundColor: colours.white}}>
//             <TouchableOpacity  onPress={onPress} style={{flexDirection: 'row',justifyContent: 'flex-start',alignItems: "center",flex : 1,marginTop : 25,padding: 5,marginLeft:5}} >
//               {/* <TouchableOpacity  onPress={onPress} > */}
//                {isBackIconVisible ?  <Icon name='ios-arrow-back' style={{ color: colours.black}} /> : <Icon name='md-list' style={{ color: colours.black}} />} 
//               {/* </TouchableOpacity> */}
//               { istextVisible && <H1 style = {styles.textStyle} > {getHeaderText} </H1>
//               }
//             </TouchableOpacity>
//             { istextVisible && <View  style={{borderBottomColor: colours.lightgray, borderBottomWidth: 1}} />
//             }
//           </View>
//         );
//     };

const styles = {
    textStyle:{
        color : colours.white,
        fontWeight: 'bold',
        paddingLeft: 15,
        paddingTop: 3,
    },
    imageContainer: {
      width: null,
      height: 85
    }
};

export default CustomHeader;