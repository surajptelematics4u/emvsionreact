import React,{Component} from 'react';
import {  View,TouchableOpacity } from 'react-native';
import { Icon,H1,H3 } from 'native-base';

import colours from "../theme/color";


const CustomHeader = ({onPress,onPressmenu,getHeaderTextone,getHeaderTexttwo,isSerIconVisible,serIconName}) => {
  return (
    <View style={styles.imageContainer}>
    <View style={{height: 60,flex : 1,flexDirection: 'row',justifyContent: 'space-between',alignItems: "flex-end"}}>
          <TouchableOpacity  onPress={onPress} style={{flexDirection: 'row',justifyContent: 'flex-start',alignItems: "center",flex : 1,padding: 10}} >
              <Icon name='arrow-back' style={{ color: colours.black}} />
              <View style={{flexDirection: 'column',justifyContent: 'center'}}>
                 <H1 style = {styles.textStyle} > {getHeaderTextone} </H1>
                 <H3 style = {styles.textStyle} > {getHeaderTexttwo} </H3>
              </View>
          </TouchableOpacity>
          {isSerIconVisible &&   <TouchableOpacity onPress={onPressmenu}  style={{alignItems: "flex-end",justifyContent: 'center'}} >
             <Icon name={serIconName} style={{ color: colours.primaryColour,alignItems: "flex-end",justifyContent: 'center',padding: 10}} />
           </TouchableOpacity>
          }
       
        </View>
    </View>
  );
};

const styles = {
    textStyle:{
        color : colours.black,
        fontWeight: 'bold',
        paddingLeft: 15,
        paddingTop: 3,
    },
    imageContainer: {
      width: null,
      height: 85,
      backgroundColor : 'rgba(255,255,255, 0.6)'
    }
};

export default CustomHeader;