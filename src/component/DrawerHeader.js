import React, { Component } from 'react';
import { View, Text,StyleSheet,Image,AsyncStorage,FlatList,ScrollView,TouchableOpacity,ImageBackground  } from 'react-native';
import { Container, Content, Icon, Header, Body,ListItem,Right,Left,Grid,Row,Col,Thumbnail,H1,H2,H3  } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';

import { drawerNavigation,userName,loginData } from "../util/Constant";
import colours from "../theme/color"

const headerback = require("../assets/image/headerback.png");

const Dashboard_icon = require("../assets/image/Dashboard_icon.png");
const Dashboard_icon_grey = require("../assets/image/Dashboard_icon_grey.png");
const live_vision_icon = require("../assets/image/live_vision_icon.png");
const live_vision_grey = require("../assets/image/live_vision_grey.png");

class DrawerHeader extends Component {
      constructor(props) {
        super(props);
        this.state = {
          datas : [],
          selected: '',
          loadeditem:false,
          userNmae : 'Guest'
        };
    }

    moveToLogout = () => {
    //  this.props.navigation.navigate('LogOut');
        AsyncStorage.setItem(loginData, JSON.stringify({loginStatus: false}));
        this.props.navigation.replace('Login');
        // const resetAction = StackActions.reset({
        //   index: 0,
        //   key: null,
        //   actions: [NavigationActions.navigate({ routeName: 'Login'})],
        // });
        // this.props.navigation.dispatch(resetAction);
    }
    
    componentWillMount(){ 
        AsyncStorage.getItem(drawerNavigation)
          .then((result) => {
            let drawerList = [{ "Name":"No Available","Id":'NoAvailable'}];
            try { 
              drawerList = JSON.parse(result);
              !this.isCancelled && this.setState({datas : drawerList,selected : drawerList[0].Id ,loadeditem : true});
              AsyncStorage.getItem(userName)
              .then((userNameRes) => {
                  this.setState({userNmae : JSON.parse(userNameRes)});
              });
            }
            catch(err) {}
            finally {
              this.props.navigation.navigate(drawerList[0].Id);
            }
          });
    }

    componentWillUnmount () {
      this.isCancelled = true;
    }

    _onListPress = (item) => {
         this.setState({ selected: item.Id });
         this.props.navigation.navigate(item.Id);
    }



    render() {
        return (
      <Container >
        <Grid>
          <Row size={2} >
          <ImageBackground source={headerback} style={{flex:1,justifyContent: 'flex-end',alignItems: 'flex-start'}}>
            <H1  style={{color: colours.white,padding: 15,fontSize: 22,fontWeight: '200'}}>
                    {this.state.userNmae}
            </H1>
            </ImageBackground>
          </Row>
          <Row size={7} style={{borderBottomColor: colours.lightgray, borderBottomWidth: 1}}>
          <ScrollView style={{flex : 1}} >
          { this.state.loadeditem && <FlatList
            data={this.state.datas}
            extraData={this.state}
            keyExtractor={(item, index) => String(index)}
            renderItem={({ item,index }) => {

              let component = null;
              switch(item.Id) {
                case "SLADashboard":
                  if(this.state.selected === "SLADashboard"){
                    component =  <Image  source= {Dashboard_icon} style={{ height:30,width:30}} />
                  }else{
                    component =  <Image  source= {Dashboard_icon_grey} style={{ height:30,width:30}} />
                  }
                  break;
                case "LiveVision":
                  if(this.state.selected === "LiveVision"){
                    component =  <Image  source= {live_vision_icon} style={{ height:30,width:30}} />
                  }else{
                    component =  <Image  source= {live_vision_grey} style={{ height:30,width:30}} />
                  }                 
                 break;
                default:
                  component = <Image  source= {Dashboard_icon_grey}style={{ height:30,width:30}} />
              }

              return (
                <ListItem
                  selected={this.state.selected === item.Id}
                  onPress={() => this._onListPress(item)}
                  style={[{flex :1,borderBottomWidth: 0,marginLeft : 0,paddingLeft :15 },this.state.selected == item.Id ? styles.isSelected : styles.notSelected]}
                >  
                <View style={{justifyContent: 'flex-start',alignItems: "flex-start",flexDirection: 'row'}} onPress={() =>  this.moveToLogout() }  >
                    {component}
                    <H2 style={[{marginLeft:10,marginTop:4},this.state.selected == item.Id ? {color: colours.primaryColour} : {color: colours.black}]}>
                     {item.Name}
                    </H2>
                </View>  
                </ListItem>
              );
              }}
            />
            }
            </ScrollView >
          </Row>
          <Row size={1} style={{padding:15,borderBottomColor: colours.lightgray, borderBottomWidth: 1}}>
          <View style={{flexDirection: 'column',flex : 1}} >
            <TouchableOpacity style={{justifyContent: 'flex-start',alignItems: "flex-start",flexDirection: 'row'}} onPress={() =>  this.moveToLogout() }  >
              <Icon name="log-out" style={{ color: colours.red400}} />
              <H2 style={{marginLeft:10,marginTop:4}}>
                Log Out
              </H2>
            </TouchableOpacity>
            <View style={{flexDirection: 'row',marginTop : 10,justifyContent: 'space-between'}}>
                  <H3  style={{padding : 5,justifyContent: 'center',alignItems: "flex-start"}}>
                   Version
                  </H3>
                  <H3 style={{padding :5,alignItems: "flex-end",justifyContent: 'center'}}>
                   1.0.0v
                  </H3>
            </View>
          </View>
          </Row>
        </Grid>
      </Container>
        );
    }
}

const styles = StyleSheet.create({
    drawerHeader: {
      height: 150,
      backgroundColor : colours.white
    },
    drawerImage: {
      height: 80,
      width: 80
    },
    isSelected:{
     // color : colours.primaryColour,
      backgroundColor : colours.verylightgray
    },
    notSelected:{
      
    }
    
  
  });

export default DrawerHeader;


