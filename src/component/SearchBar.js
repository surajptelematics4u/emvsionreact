import React, {Component} from 'react';
import Dimensions from 'Dimensions';
import {StyleSheet, View, TextInput, Image,TouchableOpacity,ActivityIndicator} from 'react-native';
import { Container,Content,Fab,H2,Toast } from 'native-base';
const deleteImg = require("../assets/image/delete.png");
const serarchImg = require("../assets/image/search.png");

import colours from "../theme/color";

export default class SearchBar extends Component {

  componentDidMount(){
    this.textInput.focus();
  }

  setText(value) {
    this.setState({
      text: value
    });
    try {
      return this.props.onChangeTextValue(value);
    } catch (_error) { }
  }

  clearText() {
    this.textInput.clear()
    try {
      return this.props.onClearTextValue();
    } catch (_error) { }
  }

  render() {
    return (
    <Container style={{backgroundColor:  colours.primaryColour}}>
      <View style={{flex: 1,justifyContent: 'space-between',alignItems: 'center',flexDirection:'row',backgroundColor:  colours.white,marginRight: 10,marginBottom: 10,marginLeft: 10}}>
         <View style={{flex: 1,flexDirection:'row',alignItems: 'center'}}>
          <Image source={serarchImg} style={styles.inlineImg} />
          <TextInput
            ref={input => { this.textInput = input }}
            style={{flex: 1}}
            placeholder={this.props.placeholder} 
            secureTextEntry={this.props.secureTextEntry}
            autoCorrect={this.props.autoCorrect}
            autoCapitalize={this.props.autoCapitalize}
            returnKeyType={this.props.returnKeyType}
            placeholderTextColor="gray"
            underlineColorAndroid="transparent"
            onChangeText={(value) => this.setText(value)}
            maxLength={this.props.maxLength}
          /> 
        </View>
        <View style={{flexDirection:'row',alignItems: 'center'}}>
          {this.props.textprogressVisible && <ActivityIndicator size="small" color={colours.primaryColour} style={{marginRight: 5}} />
          }
          {this.props.clearIconVisible && <TouchableOpacity  onPress={() => this.clearText()}>
           <Image source={deleteImg} style={styles.inlineImg} />
          </TouchableOpacity>
          }
        </View>
      </View> 
    </Container>  
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  input: {
  //  backgroundColor: colours.white,
    width: DEVICE_WIDTH - 40,
    height: 10,
   // marginHorizontal: 20,
   // paddingLeft: 45,
   // borderRadius: 20,
    color: colours.black,
  },
  inputWrapper: { 
    height: 40,
    margin: 10,
    backgroundColor: colours.white,
    justifyContent: 'center',
    alignItems: "flex-start",
    flexDirection: 'row',
   
  },
  inlineImg: {
    width: 22,
    height: 22,
    margin: 5,
  },
});
