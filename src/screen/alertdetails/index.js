import React, { Component } from 'react';
import {StyleSheet,ImageBackground,Image,View,TouchableOpacity } from 'react-native';
import { Container,H3,Fab,H2,Icon,Text } from 'native-base';
import MapView, { Marker,PROVIDER_GOOGLE } from 'react-native-maps';
import styles from "./styles";
//import FastImage from '../../component/FastImage';
//const Image = createImageProgress(FastImage);
import CustomSubHeader from '../../component/CustomSubHeader';
import { mapStylejson } from "../../theme/mapstyles";
import colors from '../../theme/color'; 
import { MARKERWIDTH,MARKERHEIGHT } from "../../util/MapUtil";
const alertpoint = require("../../assets/image/alertpoint.png");
const location = require("../../assets/image/area_icon.png");


import { LATITUDE,LONGITUDE,LATITUDE_DELTA,LONGITUDE_DELTA,LATITUDE_DELTASUB,LONGITUDE_DELTASUB } from "../../util/MapUtil";

class AlertDetails extends Component {

  constructor(props) {
    super(props);

    const { navigation } = this.props;

    let markerVisibled = true;
    let longituded =  navigation.getParam('longituded', '80.110483');
    let latituded = navigation.getParam('latituded', '22.062867');

    if(longituded === '80.110483' && latituded === '22.062867'){
      markerVisibled = false;
    }

    console.log(longituded+ ' '+ latituded);
 
    this.state = {
      startMarker:{longitude :80.110483,latitude :22.062867 },
      itemId : navigation.getParam('itemId', 'NO-ID'),
      itemIdVec : navigation.getParam('itemIdVec', 'NO-ID'),
      otherParam :navigation.getParam('otherParam', ''),
      loginData :navigation.getParam('loginData', ''),
      alertName :navigation.getParam('alertName', ''),
      longitude : parseFloat(longituded),
      latitude :parseFloat(latituded),
      markerVisible : markerVisibled,
    };
    
  }

  componentWillMount(){
  }

   fitBottomTwoMarkers = () => {
    // this.map.fitToSuppliedMarkers(['Alert',''], true);
     this.map.animateToRegion({
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      latitudeDelta: LATITUDE_DELTASUB,
      longitudeDelta: LONGITUDE_DELTASUB,
    }, 1000);
   }

  render() {

    return (
      <Container>
        <MapView
          ref={ref => { this.map = ref; }}
          provider={PROVIDER_GOOGLE}
          style={{...StyleSheet.absoluteFillObject}}
          zoomEnabled={true}
          showsUserLocation = {false}
          followUserLocation = {false}
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
          customMapStyle={mapStylejson}
          onMapReady={() => this.fitBottomTwoMarkers()}
        > 
         {this.state.markerVisible && <Marker 
                image={alertpoint}
                identifier='Alert'
                key='Alert'
                coordinate={{longitude :this.state.longitude,latitude :this.state.latitude}}
              > 
             {/* <ImageBackground  source={alertpoint}  style={{ width: 30, height: 30 }} />  */}
          </Marker>
         }
        </MapView>
        {/* <View style={{ flex: 1 }}> */}
          <CustomSubHeader  
            getHeaderTextone={'Alert Details'} 
            getHeaderTexttwo={'Trip ID : '+this.state.itemId}
           // isSerIconVisible={''} 
            onPress={() => this.props.navigation.goBack()}  >
          </CustomSubHeader>
          <View style={{backgroundColor: colors.white  , position: 'absolute',bottom:0,right:0,left:0, justifyContent: 'flex-end' }}>
            <TouchableOpacity onPress={() => this.setState({alertDetaildialogVisible: false})} style={{flexDirection: 'row',justifyContent: 'space-between',paddingRight: 15,paddingTop: 15,paddingLeft: 10}} >
                <View style={{backgroundColor: colors.red500, borderRadius: 20, paddingVertical: 6, paddingHorizontal: 12}}>
                <H2 style={{color: colors.white,fontWeight: '200'}} >
                 {this.state.alertName}
                </H2>
                </View>
                <View  style={{justifyContent: 'center'}} >
                <H3>
                  {this.state.otherParam.DateandTime}
                 </H3>
                </View>
            </TouchableOpacity>
            <View style={{  flex: 1, flexDirection: 'column'}}>
                   <H2 style={{ fontWeight: "bold",padding: 15}}>
                     {this.state.otherParam.VehicleNumber}
                        </H2>
                    <View style={{flex: 1, flexDirection: 'row',paddingRight:15,paddingLeft:15}}>
                        <Image source= {location} style={styles.otherIcon}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3 style={{marginBottom: 90}} >{this.state.otherParam.Location}</H3> 
                        </View>
                    </View> 
              </View>
          </View>
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: colors.white }}
            position="bottomRight"
            onPress={() => this.fitBottomTwoMarkers()}>
            <Icon name='md-locate' style={{color: colors.black}} />
          </Fab>
        {/* </View> */} 
      </Container>
    );
  }
}

export default AlertDetails;
