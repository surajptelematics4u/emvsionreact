import React, { Component } from 'react'
import { Image,View,TouchableOpacity,StatusBar,ActivityIndicator,Dimensions } from 'react-native';
import { Card,H3,H2,Container,Content,Button,Text } from 'native-base';
import EasyListView from '../../component/list/EasyListView';
import styles from "./styles";
import { alertsDetailsAPI } from '../../actions/ApiAction';
import fetchData from '../../component/CancelableFetch';
import  Dialog  from '../../component/dialogs/Dialog';

import CustomHeader from '../../component/CustomHeader';
import colors from "../../theme/color";

const idle = require("../../assets/image/location_icon_blue.png");

const location = require("../../assets/image/area_icon.png");


class AlertList extends Component {

  // static defaultProps = {
  //   empty: false,
  //   error: false,
  //   noMore: false,
  //   column: 1
  // }

  constructor(props) {
    super(props)

    const { navigation } = this.props;

    this.state = {
     // startprogress:true, 
      isComponentNotActive: false,
      nodatadialogVisible: false,
      itemId : navigation.getParam('itemId', 'NO-ID'),
      otherParam :navigation.getParam('otherParam', ''),
      loginData :navigation.getParam('loginData', '')
    } 
    this.renderListItem = this._renderListItem.bind(this) 
    this.onFetch = this._onFetch.bind(this)
  }

  componentWillUnmount () {
    fetchData.abort(6);
  }

  _onListPress = (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('AlertDetails', {
      itemId: rowData.TripID,
      itemIdVec: rowData.VehicleNumber,
      otherParam: rowData,
      loginData: this.state.loginData,
      alertName: this.state.otherParam.alertName,
      latituded: rowData.latitude,
      longituded: rowData.longitude
    });
   };

  render() {
    const footerHeight = Dimensions.get('screen').height/2;
    return (
      <Container>
        <StatusBar barStyle="light-content" />  
      <CustomHeader  getHeaderText={this.state.otherParam.alertName} isbackVisible={true} onPress={() => this.props.navigation.goBack()}   />
      {/* <Content >
      {this.state.startprogress && <View style={{width: null,height:footerHeight,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
      } */}
       <EasyListView
        ref={component => this.listview = component}
        column={1}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetch}
        emptyContent={'No record found.'}
      />
      {/* </Content> */}
      <Dialog 
          visible={this.state.nodatadialogVisible} 
          >
          <View>
            <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
              <View  style={{padding:10}}>
              <H2 style={{fontWeight: '100',textAlign: 'center'}} >
              No Records Available
              </H2> 
              </View>
              <Button full rounded  primary style={{margin :10}}    onPress={() => this.props.navigation.goBack()} >
                <Text style={{color: colors.white}} >CLOSE</Text>
              </Button>
            </View>
          </View>
      </Dialog>  
    </Container>
    )
  }
  _renderListItem = (rowData, sectionID, rowID, highlightRow) => {
    return (
      <TouchableOpacity id={rowData}  onPress={() => this._onListPress(rowData)} >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                    <Image  source= {idle} style={styles.listIcon}/>
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                            {rowData.VehicleNumber}
                        </H2>
                        <H3 style={{ fontSize:10}}>
                            {rowData.TripID}
                        </H3>
                    </View>
                    <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3>
                            {rowData.DateandTime}
                        </H3>
                    </View>
                  </View>
                    <View style={styles.listItemContainer3}>
                        <Image source= {location} style={styles.otherIcon}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3>{rowData.Location}</H3>
                        </View>
                    </View> 
                  </View>
                </Card>
        </TouchableOpacity>
    )
  }

  getDataFromApi = (pageNo, success, failure) => {
    alertsDetailsAPI({alertId: this.state.itemId,PageNo:pageNo, loginData : this.state.loginData })
        .then((res) => {
         // this.setState({startprogress :false});
        //  if(!this.state.isComponentNotActive){
              if (typeof res.vehicleDetails !== 'undefined' && res.vehicleDetails.length > 0) {
                success(res.vehicleDetails);
              }else{
                this.setState({nodatadialogVisible: true});
              }
          //  }else{
          //   failure('loding fail...', null); 
          // } 
        })
        .catch((error) => {
         // this.setState({startprogress :false});
          failure('connectionError', null);
        });
  }

  _onFetch(pageNo, success, failure) {
    // if (this.props.empty) {
    //     success([]);
    //   }
    //   else if (this.props.error) {
    //     if (pageNo === 1) {
    //       this.getDataFromApi(pageNo, success, failure);
    //     }
    //     else {
    //       failure('Unable to connect to server.', null);
    //     }
    //   }
    //   else if (this.props.noMore) {
    //     if (pageNo === 1) {
    //       this.getDataFromApi(pageNo, success, failure);
    //     }
    //     else {
    //       success([])
    //     }
    //   }
    //   else {
        this.getDataFromApi(pageNo, success, failure);
   //   }
  }
}

export default AlertList;
