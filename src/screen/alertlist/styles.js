import React,{ StyleSheet } from 'react-native';
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const devicewidth = Dimensions.get("window").width;
import color from "../../theme/color"


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
        backgroundColor: '#ecf0f1',
      },
      listItem: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#d6d7da',
        padding: 6,
      },
      imageWrapper: {
        padding: 5,
      },
      title: {
        textAlign: 'left',
        margin: 6,
      },
      subtitle: {
        textAlign: 'left',
        margin: 6,
      },
      container: {
        flex: 1,
        marginTop:20,
      },
        listIcon:{
          height:25,
          width:25
        },
        otherIcon:{
          height:15,
          width:15
        },
        listItemContainer:{
         flex: 1,
         flexDirection: 'column',
        },
        ItemContainer1:{
          flex:2,
          flexDirection: 'row',
         },
        ItemContainer2:{
          paddingLeft:5,
          paddingTop: 10,
          flex: 1,
          flexDirection: 'row'
         },
         listItemContainer3:{
          paddingLeft:5,
          flex: 1,
          flexDirection: 'row',
          paddingTop: 10,
         },
        VehicleName:{
          fontWeight: "bold",
         },  
      TextInputStyleClass:{     
        textAlign: 'center',
        height: 40,
        borderWidth: 1,
        borderColor: '#009688',
        borderRadius: 7 ,
        backgroundColor : "#FFFFFF"
             
        }
});
export default styles;