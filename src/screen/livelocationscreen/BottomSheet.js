import React, { Component } from 'react';
import {View,Animated,Dimensions,StyleSheet,TouchableOpacity,Image,Text,ImageBackground,TouchableWithoutFeedback} from 'react-native';
import {Header,H3,Card,Toast} from 'native-base';
const menu = require("../../assets/image/menu_bg.png");
const down = require("../../assets/image/menu_icon_down.png");
const livelocation = require("../../assets/image/live_location_icon.png");
const last24hrs = require("../../assets/image/last_24hrs_icon.png");

const { width, height } = Dimensions.get('window');

export default class BottomSheet extends Component {

  state = {
    position: new Animated.Value(this.props.isOpen ? 0 : height), 
    visible: this.props.isOpen,
  };
 
  componentWillReceiveProps(nextProps) { 
    if (!this.props.isOpen && nextProps.isOpen) {
      this.animateOpen();
    } 
    else if (this.props.isOpen && !nextProps.isOpen) {
      this.animateClose();
    }
  }
 
  animateOpen() { 
    this.setState({ visible: true }, () => { 
      Animated.timing(
        this.state.position, { toValue: 0 }     // top of the screen
      ).start();
    });
  }

  // Close popup
  animateClose() { 
    Animated.timing(
      this.state.position, { toValue: height }  // bottom of the screen
    ).start(() => this.setState({ visible: false }));
  }

  _onListPress = (item) => {
    this.props.menuSelection(item);    
   };

  render() { 
    if (!this.state.visible) {
      return null;
    }
    return (
      <View style={styles.container}> 
        <TouchableWithoutFeedback onPress={this.props.onClose}>
          <Animated.View style={styles.backdrop}/>
        </TouchableWithoutFeedback>
        <Animated.View
          style={[styles.modal, { 
            transform: [{ translateY: this.state.position }, { translateX: 0 }]
          }]}
        >
        <ImageBackground  source={menu} style={styles.imageContainer}>
           <Card style={ {  width: '80%',
           height: '90%',
           marginLeft: '10%',
           marginRight: '10%',
           marginTop: '10%',
           backgroundColor: 'transparent',
           marginBottom: '10%'}}>
         
            {/* <TouchableOpacity style={styles.center}  onPress={() => this.props.navigation.navigate('LiveLocation')}>
            <Image source={down} style={styles.otherIcon} />
            </TouchableOpacity> */}
            
            <View style={ {flex:1}}>    
              <View style={{paddingTop:25, flexDirection: 'column', justifyContent: 'center',  alignItems: 'center'}}>
                <TouchableOpacity  onPress={() => this._onListPress(1)} >
                <View style={{ justifyContent: 'center',  alignItems: 'center'}}>
                <Image  source={livelocation} style={styles.menuIcon}></Image>
               </View>
                <H3 style={{fontSize:15}}>Live Location</H3>
                </TouchableOpacity>
              </View>
              <View style={{paddingTop:25, flexDirection: 'column', justifyContent: 'center',  alignItems: 'center'}}>
                <TouchableOpacity  onPress={() => this._onListPress(2)}>
                <View style={{ justifyContent: 'center',  alignItems: 'center'}}>
                <Image  source={last24hrs} style={styles.menuIcon}></Image>
               </View>
                <H3 style={{fontSize:15}}>Last 24 hrs Activity</H3>
                </TouchableOpacity>
              </View>
          </View>  
          </Card>
        </ImageBackground>
        </Animated.View>
      </View>
    );
  }

}

const styles = StyleSheet.create({ 
  container: {
    ...StyleSheet.absoluteFillObject,   // fill up all screen
    justifyContent: 'flex-end',         // align popup at the bottom
    backgroundColor: 'transparent',     // transparent background
  }, 
  backdrop: {
    ...StyleSheet.absoluteFillObject,   // fill up all screen
    backgroundColor: 'black',
    opacity: 0.5,
  }, 
  modal: {
    height: height,                 // take half of screen height
    backgroundColor: 'white',
  },
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  otherIcon:{
    height:48,
    width:48,
  },
  menuIcon:{
    width: 30,
    height: 36,
  },
  listItemContainer:{
    flexDirection: 'column',
  },
  title: {
    fontSize: 20,
  },
  center:{
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
});


// <DetailWindow
//               //popupData={this.state.popupData}
//               isOpen={this.state.popupIsOpen}
//               onClose={this.closPopup}
//             />

//             openPopup = () => {
//               // if(this.state.popupData.length > 0){
//            this.setState({
//              popupIsOpen: true,
//                //   popupData,	
//            });
//            //    }
//          }
//          closPopup = () => {
//            this.setState({
//              popupIsOpen: false,
//            });
//          }
       