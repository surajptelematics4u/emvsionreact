import React, { Component } from 'react';
import { View, Text,StyleSheet,ActivityIndicator,Image,ImageBackground } from 'react-native';
import { Container,H3,Fab,H2,Icon } from 'native-base';
import MapView, { Marker,PROVIDER_GOOGLE } from 'react-native-maps';
import { mapStylejson } from "../../theme/mapstyles";
import { currentLocationDataAPI } from '../../actions/ApiAction';
const DEFAULT_PADDING = { top: 150, right: 150, bottom: 150, left: 150 };

import { LATITUDE,LONGITUDE,LATITUDE_DELTA,LONGITUDE_DELTA,LATITUDE_DELTASUB,LONGITUDE_DELTASUB } from "../../util/MapUtil";
import colors from '../../theme/color'; 

const currentlocation = require("../../assets/image/current_location.png");
const location = require("../../assets/image/area_icon.png");

const idle = require("../../assets/image/location_icon_blue.png");
const stoppage = require("../../assets/image/location_icon_black.png");
const running = require("../../assets/image/location_icon_green.png");

class CurrentLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
       refreshing: true,
        longitude :80.110483,
        latitude :22.062867 ,
        title : 'Current Location',
        description : '',
        Date:'',
        Speed: '',
        VehicleStatus: 'idle',
        markerShow : false
    };
  }

  componentDidMount(){
    this.getCurrentLocationDataAPI();
  //  this.interval = setInterval(()=>this.getCurrentLocationDataAPI(), 10000);      
  }

  componentWillUnmount() {
  //  clearInterval(this.interval)
  }

  getCurrentLocationDataAPI = () => {
    let loginData = this.props.loginData;
    let VehicleNo = this.props.VehicleNo;
    currentLocationDataAPI({VehicleNo: VehicleNo, loginData : loginData })
        .then((res) => {
           this.setState({refreshing :false});
          if (typeof res.vehicleDetails !== 'undefined' && res.vehicleDetails.length > 0){
            this.setState({markerShow : true});

            this.setState({longitude :res.vehicleDetails[0].longitude
            , latitude :res.vehicleDetails[0].latitude
            , description :res.vehicleDetails[0].Location
            , title :res.vehicleDetails[0].Vehicle});

             this.setState({Date :res.vehicleDetails[0].Date
              ,Speed: res.vehicleDetails[0].Speed
              ,VehicleStatus: res.vehicleDetails[0].VehicleStatus});
  
             this.fitBottomTwoMarkers();

            setTimeout(() => {
              this.getCurrentLocationDataAPI();
            },10000);
          }
        })
        .catch((error) => {
           this.setState({refreshing :false,markerShow : false});
        //  failure('Unable to connect to server.', null);
        });
  }

  fitBottomTwoMarkers = () => {
    //this.map.fitToSuppliedMarkers(['CurrentLocation',''], true);
    // this.map.fitToCoordinates([{longitude : this.state.longitude ,latitude: this.state.latitude }], {
    //   edgePadding: DEFAULT_PADDING,
    //   animated: true,
    // });

      this.map.animateToRegion({
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        latitudeDelta: LATITUDE_DELTASUB,
        longitudeDelta: LONGITUDE_DELTASUB,
      }, 1000);
  }

  render() {

    let component = null;
    switch(this.state.VehicleStatus) {
      case 'stoppage':
        component =  <Image  source= {stoppage} style={{ height:25,width:25}} />
        break;
      case 'running':
        component = <Image  source= {running} style={{ height:25,width:25}} />
        break;
      default:
        component = <Image  source= {idle} style={{ height:25,width:25}} />
    }

    return (
        <Container>
            <MapView
            ref={ref => { this.map = ref; }}
            provider={PROVIDER_GOOGLE}
            style={{...StyleSheet.absoluteFillObject}}
            zoomEnabled={true}
            showsUserLocation = {false}
            followUserLocation = {false}
            initialRegion={{
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }}
            customMapStyle={mapStylejson}
            >
              {!this.state.refreshing &&   <Marker
                image={currentlocation}
                identifier="CurrentLocation"
                coordinate={{latitude:this.state.latitude,longitude: this.state.longitude}}
                title={this.state.title}
                description={this.state.description}
                >
                {/* <ImageBackground source={currentlocation} style={{ width: 30, height: 30 }} /> */}
             </Marker>
              }
            </MapView>
          
            {!this.state.refreshing && <View style={{backgroundColor: colors.white  , position: 'absolute',bottom:0,right:0,left:0, justifyContent: 'flex-end' }}>
                <View style={{  flex: 1, flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row',padding:15}}>
                        <View>
                          {component}
                        </View>
                        <View style={{paddingLeft:5,flexDirection:'column'}}>
                          <H2 style={{fontWeight: "bold"}}>
                          {this.state.title}
                            </H2>
                        </View>
                        <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                            <H3 >
                            {this.state.Date}
                            </H3>
                        </View>
                      </View>
                      <View style={{flex: 1, flexDirection: 'row',paddingRight:15,paddingLeft:15}}>
                            <Image source= {location} style={{height:15,width:15}}>
                            </Image>
                            <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                            <H3 >{this.state.description}</H3> 
                            </View>
                      </View>
                      <View style={{flex: 1,marginBottom: 50,marginTop:20, flexDirection: 'column',alignItems: 'flex-start',paddingRight:15,paddingLeft:15}}>
                          <H2 style={{fontWeight: "bold",color:colors.red500}}>
                          SPEED
                          </H2>
                          <H2>
                          {this.state.Speed}
                          </H2>
                      </View>
                  </View>
              </View>
              }
              {!this.props.popupIsOpen && 
                <Fab
                  active={this.state.active}
                  style={{ backgroundColor: colors.white }}
                  position="bottomRight"
                  onPress={() => this.fitBottomTwoMarkers()}>
                  <Icon name='md-locate' style={{color: colors.black}} />
                </Fab> 
              }
              {this.state.refreshing && <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
               }
        </Container>
    );
  }
}

export default CurrentLocation;
