import React, { Component } from 'react';
import { StyleSheet, FlatList, View,TouchableOpacity } from 'react-native';
import { Button,Text,Container,H3,H1,H2,Content,Icon,Input,Left,Card,Body,CardItem,Form,Item,Toast } from "native-base";

import styles from './styles';
import colors from '../../theme/color'; 
import Last24HrsListTab from './Last24HrsListTab';
import Last24HrsMapTab from './Last24HrsMapTab';

class Last24HrsActivity extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isSelectedTab : 1
    };
  }

  handleMenuSelection= (item) => {
    this.setState({isSelectedTab: item});
  }

  render() {
    var self = this;
    return (
      <Container>
       
        {/* <View style={{ flex: 1,flexDirection:'column' }}> */}
          
           <View style={{...StyleSheet.absoluteFillObject}} >
            { this.state.isSelectedTab == 1 &&
               <Last24HrsMapTab popupIsOpen ={this.props.popupIsOpen} loginData={this.props.loginData} VehicleNo={this.props.VehicleNo} Hours={this.props.Hours}  />
              }
           { this.state.isSelectedTab == 2 &&
               <Last24HrsListTab popupIsOpen ={this.props.popupIsOpen} loginData={this.props.loginData} VehicleNo={this.props.VehicleNo} Hours={this.props.Hours}  />
            }
        </View>
        <View  style={{position: 'absolute',bottom:0,right:0,left:0, flexDirection: 'row',justifyContent: 'center',alignItems: 'center' }}  >
            <View style={{flex : 1,flexDirection: 'row',paddingTop:15,paddingBottom:15,backgroundColor:colors.white}}>
                <TouchableOpacity onPress ={() => this.setState({isSelectedTab : 1})}  style={[this.state.isSelectedTab == 3 ? styles.isSelected : styles.tabStyle]} > 
                  <H2 style={[this.state.isSelectedTab == 1 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`Map View`}
                  </H2>
                </TouchableOpacity>
                <TouchableOpacity  onPress ={() => this.setState({isSelectedTab : 2})} style={[this.state.isSelectedTab == 4 ? styles.isSelected : styles.tabStyle]} > 
                  <H2 style={[this.state.isSelectedTab == 2 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`List View`}
                  </H2>
                </TouchableOpacity>
              </View>
          </View>
        {/* </View> */}
      </Container>
    );
  }
}

export default Last24HrsActivity;