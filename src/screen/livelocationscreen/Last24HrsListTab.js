import React, { Component } from 'react'
import { Image,View,TouchableOpacity,ListView,ActivityIndicator,Dimensions } from 'react-native';
import { Card,H3,H2,Container,Content } from 'native-base';
import EasyListView from '../../component/list/EasyListView'
import styles from "./styles";
import { last24hourslistviewAPI } from '../../actions/ApiAction';

import CustomHeader from '../../component/CustomHeader';
import colors from "../../theme/color";

const distanceicon = require("../../assets/image/distance_icon.png");
const location = require("../../assets/image/area_icon.png");

const idle = require("../../assets/image/location_icon_blue.png");
const stoppage = require("../../assets/image/location_icon_black.png");
const running = require("../../assets/image/location_icon_green.png");

class Last24HrsActivity extends Component {

  static defaultProps = {
    empty: false,
    error: false,
    noMore: false,
    column: 1
  }

  constructor(props) {
    super(props)

    const { navigation } = this.props;

    this.state = {
      startprogress:true
    } 
    this.renderListItem = this._renderListItem.bind(this) 
    this.onFetch = this._onFetch.bind(this)
  }

  render() {
    const footerHeight = Dimensions.get('screen').height/2;
    return (
      <Container  style={{marginTop: 90,marginBottom:50}} >
       <EasyListView
        ref={component => this.listview = component}
        column={1}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetch}
        emptyContent={'No record found.'}
      />

      {/* {this.state.startprogress && <View style={{width: null,height:footerHeight,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
      } */}
    </Container>
    )
  }
  _renderListItem(rowData, sectionID, rowID, highlightRow) {
    let component = null;
    switch(rowData.VehicleStatus) {
      case 'STOPPAGE':
        component =  <Image  source= {stoppage} style={styles.listIcon} />
        break;
      case 'RUNNING':
        component = <Image  source= {running} style={styles.listIcon}/>
        break;
      default:
        component = <Image  source= {idle} style={styles.listIcon}/>
    }

    return (
      <View id={rowData}  >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                     {component}
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                        {rowData.Date} 
                        </H2>
                    </View>
                    {/* <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3>
                        {rowData.Vehicle}
                        </H3>
                    </View> */}
                  </View>
                    <View style={[styles.listItemContainer3,{marginTop: 10}]}>
                        <Image source= {location} style={[styles.otherIcon]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3>{rowData.Location}</H3>
                        </View>
                    </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {distanceicon} style={[styles.otherIcon,{marginTop: 10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10}}>
                        <H3  >{rowData.Speed}</H3>
                        </View>
                    </View>  
                  </View>
                </Card>
        </View>
    )
  }

  getDataFromApi = (pageNo, success, failure) => {
    let loginData = this.props.loginData;
    let VehicleNo = this.props.VehicleNo;
    let Hours = this.props.Hours;
    last24hourslistviewAPI({VehicleNo: VehicleNo,Hours: Hours,PageNo:pageNo, loginData : loginData })
        .then((res) => {
          this.setState({startprogress :false});
          success(res.ActivityDetails);
        })
        .catch((error) => {
          this.setState({startprogress :false});
          failure('connectionError', null);
        });
  }

  _onFetch(pageNo, success, failure) {
    if (this.props.empty) {
        success([]);
      }
      else if (this.props.error) {
        if (pageNo === 1) {
          this.getDataFromApi(pageNo, success, failure);
        }
        else {
          failure('Unable to connect to server.', null);
        }
      }
      else if (this.props.noMore) {
        if (pageNo === 1) {
          this.getDataFromApi(pageNo, success, failure);
        }
        else {
          success([])
        }
      }
      else {
        this.getDataFromApi(pageNo, success, failure);
      }
  }
}

export default Last24HrsActivity;
