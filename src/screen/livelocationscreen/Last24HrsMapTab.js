import React, { Component } from 'react';
import { View, Text,StyleSheet,ActivityIndicator,Image,ImageBackground,TouchableOpacity,FlatList } from 'react-native';
import { Container,H3,H1,Fab,H2,Icon,Button,CheckBox} from 'native-base';
import MapView, { Marker,Polyline,PROVIDER_GOOGLE } from 'react-native-maps';
import { mapStylejson } from "../../theme/mapstyles";
import { last24hoursmapviewAPI,vehicleAlertsApi } from '../../actions/ApiAction';
const DEFAULT_PADDING = { top: 150, right: 150, bottom: 150, left: 150 };
import  Dialog  from '../../component/dialogs/Dialog';

import { LATITUDE,LONGITUDE,LATITUDE_DELTA,LONGITUDE_DELTA } from "../../util/MapUtil";
import colors from '../../theme/color'; 

const alertIcon = require("../../assets/image/warning.png");
const gps = require("../../assets/image/gps.png");
const currentlocation = require("../../assets/image/current_location.png");
const alertpoint = require("../../assets/image/alertpoint.png");


class Last24HrsMapTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nodatadialogVisible:false,
      dialogVisible: false,
      alertDetaildialogVisible: false,
      alertDetaildialogDetail: {Details:'',Name:''},
      mandatoryAlerts:[],
      AlertTripVisible: false,
      Marker:[],
      refreshing: true,
      alertIdSelected:"",
      longitude :80.110483,
      latitude :22.062867 ,
      ActivityDetails: [{longitude : 72.87004089355469, latitude : 22.11152458190918}],
      markerShow : false
    };
  }

  componentDidMount(){
    this.getlast24hoursmapviewAPI();  
  }

  getlast24hoursmapviewAPI = () => {
    let loginData = this.props.loginData;
    let VehicleNo = this.props.VehicleNo;
    let Hours = this.props.Hours;
    last24hoursmapviewAPI({VehicleNo: VehicleNo,Hours: Hours, loginData : loginData })
        .then((res) => {
          this.setState({refreshing :false});
           if (typeof res.ActivityDetails !== 'undefined' && res.ActivityDetails.length > 0){
              this.setState({markerShow : true});
              this.setState({ActivityDetails : res.ActivityDetails});
              this.setState({mandatoryAlerts : res.MandatoryAlertDetails});
              this.setState({longitude : res.ActivityDetails[0].longitude,latitude: res.ActivityDetails[0].latitude});
              this.fitBottomTwoMarkers();
            }else{
              this.setState({markerShow : false});
              this.setState({nodatadialogVisible: true});
            }
        })
        .catch((error) => {
           this.setState({markerShow : false});
           this.setState({refreshing :false});
           failure('connectionError', null);
        });
  }
  onMapPress = (marker) => { 
    this.setState({alertDetaildialogDetail: marker});
    this.setState({alertDetaildialogVisible: true});
  }
  getAlertDetails = () => {
    let loginData = this.props.loginData;
    let VehicleNo = this.props.VehicleNo;
    let Hours = this.props.Hours; 
    this.setState({refreshing: true});
    this.setState({dialogVisible: false});
    vehicleAlertsApi({alertId:this.state.alertIdSelected,Hours: Hours, loginData : loginData,VehicleNo:VehicleNo })
    .then((res) => {
      this.setState({refreshing: false});
      if (typeof res.ActivityDetails !== 'undefined' && res.ActivityDetails.length > 0){
        // let markers = [];
        // markers = res.ActivityDetails.map(marker => (<Marker
        //         image={alertpoint}
        //         key={marker.code}
        //         coordinate={{latitude: parseFloat(marker.latitude), longitude:parseFloat(marker.longitude)}}
        //         onPress={(e) => this.onMapPress(marker)}
        //     />)
        // );
        this.setState({markers :res.ActivityDetails, AlertTripVisible: true});
      }
      else{
        this.setState({refreshing: false});
        this.setState({nodatadialogVisible: true});
      }
    })
    .catch((error) => {
       this.setState({refreshing :false});
    //  failure('Unable to connect to server.', null);
    });
  }
  _onAlertListPress = (item) => {
    this.setState({ alertIdSelected: item.alertId });
  }
  r
  fitBottomTwoMarkers = () => {
    // this.map.fitToSuppliedMarkers(['source','destination'], true);
     this.map.fitToCoordinates(this.state.ActivityDetails, {
       edgePadding: DEFAULT_PADDING,
       animated: true,
     });
   }
   _onAlertListPress = (item) => {
    this.setState({ alertIdSelected: item.alertId });
  }
  render() {
   
    return (
      <Container>
      
       <MapView
            ref={ref => { this.map = ref; }}
            provider={PROVIDER_GOOGLE}
            style={{...StyleSheet.absoluteFillObject}}
            zoomEnabled={true}
            showsUserLocation = {false}
            followUserLocation = {false}
            initialRegion={{
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }}
            customMapStyle={mapStylejson}
            >
            {/* {this.state.markers} */}
          	<Polyline
              coordinates={this.state.ActivityDetails}
              strokeColor="#203d92"
              fillColor="#203d92"
              strokeWidth={3}
            />

             {this.state.markerShow &&   <Marker
                image={currentlocation}
                identifier="CurrentLocation"
                coordinate={{latitude:this.state.latitude,longitude: this.state.longitude}}
                title="Current Location"
                >
                {/* <ImageBackground source={currentlocation} style={{ width: 30, height: 30 }} /> */}
             </Marker>
              }   
           {this.state.AlertTripVisible &&  this.state.markers.map(marker => (
            <Marker
                image={alertpoint}
                key={marker.date}
                coordinate={{latitude: parseFloat(marker.latitude), longitude:parseFloat(marker.longitude)}}
                onPress={(e) => this.onMapPress(marker)}
                >
                {/* <ImageBackground source={alertpoint} style={{ width: 30, height: 30 }} /> */}
             </Marker>
            ))} 
        </MapView>
        {!this.props.popupIsOpen && 
           <Fab
              active={this.state.active}
              containerStyle={{ marginTop: 70}}
              style={{ backgroundColor: colors.white }}
              position="topRight"
              onPress={() =>  this.setState({dialogVisible: true})}>  
              <Icon name='warning' style={{color: colors.red900}} />
            </Fab>
        }
          <Dialog 
          visible={this.state.dialogVisible}  
          onTouchOutside={() => this.setState({dialogVisible: false})} >
          <TouchableOpacity   onPress={() => this.setState({dialogVisible: false})} style={{backgroundColor: colors.primaryColour,flexDirection: 'row',justifyContent: 'space-between',padding:10}} >
             <H1 style={{color: colors.white,fontWeight: '200'}} >
                Select Alert
              </H1> 
              <View  style={{justifyContent: 'center'}} >
                <Icon name="md-close" style={{ color: colors.white}} />
              </View>
          </TouchableOpacity>
          <FlatList
            data={this.state.mandatoryAlerts}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => { 
              return (
              <TouchableOpacity style={{flexDirection: 'row',marginTop:20}} onPress={() => this._onAlertListPress(item)} >
                <CheckBox 
                 onPress={() => this._onAlertListPress(item)}
                 checked={this.state.alertIdSelected === item.alertId ? true : false }
                 /> 
                <H2 style={{marginLeft: 20}} >{item.alertName}</H2> 
              </TouchableOpacity>
            );
           }}
           keyExtractor={(item, index) => String(index)}
            />
            <View style={{marginTop: 20}} >
              <Button full rounded  primary style={{margin :10} } onPress={() =>  this.getAlertDetails()}     >
                <Text style={{color: colors.white}}>SUBMIT</Text>
              </Button>
            </View> 
          </Dialog> 
          {!this.props.popupIsOpen && 
            <Fab
              active={this.state.active}
              containerStyle={{marginBottom: 40}}
              style={{ backgroundColor: colors.white }}
              position="bottomRight"
              onPress={() => this.fitBottomTwoMarkers()}>
              <Icon name='md-locate' style={{color: colors.black}} />
            </Fab>
          }
          {/* {this.state.refreshing && <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
          } */}
           {this.state.refreshing && <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
          }
          {this.state.alertDetaildialogVisible && <View style={{backgroundColor: colors.white  , position: 'absolute',bottom:0,right:0,left:0, justifyContent: 'flex-end' }}>
            <TouchableOpacity onPress={() => this.setState({alertDetaildialogVisible: false})} style={{flexDirection: 'row',justifyContent: 'space-between',paddingRight: 15,paddingTop: 15,paddingLeft: 10}} >
                <View style={{backgroundColor: colors.red500, borderRadius: 20, paddingVertical: 6, paddingHorizontal: 12}}>
                <H2 style={{color: colors.white,fontWeight: '200'}} >
                 {this.state.alertDetaildialogDetail.AlertName}
                </H2>
                </View>
                <View  style={{justifyContent: 'center'}} >
                    <Icon name="md-close" style={{ color: colors.gray600}} />
                </View>
            </TouchableOpacity>
            <H2 style={{marginLeft: 15,marginBottom: 90,marginRight: 15,marginTop: 15}} >{this.state.alertDetaildialogDetail.location}</H2> 
          </View>
          }
        <Dialog 
          visible={this.state.nodatadialogVisible} 
          >
          <View>
            <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
              <View  style={{padding:10}}>
              <H2 style={{fontWeight: '100',textAlign: 'center'}} >
              No Records Available For This Vehicle
              </H2> 
              </View>
              <Button full rounded  primary style={{margin :10}}    onPress={() =>  this.setState({nodatadialogVisible: false})} >
                <Text style={{color: colors.white}}>CLOSE</Text>
              </Button>
            </View>
          </View>
          </Dialog>  
      </Container>
    );
  }
}

export default Last24HrsMapTab;
