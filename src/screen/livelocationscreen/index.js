import React, { Component } from 'react';
import {StyleSheet,Dimensions, Image,View,TouchableOpacity } from 'react-native';
import { Container,H3,Fab,H2,Content,Text } from 'native-base';

import CustomSubHeader from '../../component/CustomSubHeader';
import colors from '../../theme/color'; 
import BottomSheet from './BottomSheet';

import CurrentLocation from './CurrentLocation';
import Last24HrsActivity from './Last24HrsActivity';
const up = require("../../assets/image/menu_icon_up.png");
const down = require("../../assets/image/menu_icon_down.png");


class LiveLocation extends Component {
  constructor(props) {
    super(props);

    const { navigation } = this.props;

    this.state = { 
      isSelectedTab : 1,
      popupIsOpen: false,
      itemId : navigation.getParam('itemId', 'NO-ID'), 
      otherParam :navigation.getParam('otherParam', ''),
      loginData :navigation.getParam('loginData', '') 
    };
  }
 
  toggalPopup = () => {
    if(this.state.popupIsOpen){
      this.setState({popupIsOpen: false});
    }else{
      this.setState({popupIsOpen: true});
    }
  }

  closPopup = () => {
    this.setState({popupIsOpen: false});
  }

  handleMenuSelection= (item) => {
    this.setState({popupIsOpen: false});
    this.setState({isSelectedTab: item});
  }

  render() {

    let component = null;
    if(this.state.popupIsOpen){
      component = <Image source={down} style={{height:48,width:48}} />
    }else{
      component = <Image source={up} style={{height:48,width:48}} />
    }

    let headerName = null;
    if(this.state.isSelectedTab == 1){
      headerName = 'Live Location';
    }else{
      headerName = 'Last 24 hrs Activity';
    }

    return (
      <Container>

        <View style={{...StyleSheet.absoluteFillObject}} >
            { this.state.isSelectedTab == 1 &&
              <CurrentLocation popupIsOpen ={this.state.popupIsOpen} loginData={this.state.loginData} VehicleNo={this.state.itemId} />
            }
           { this.state.isSelectedTab == 2 &&
              <Last24HrsActivity popupIsOpen={this.state.popupIsOpen}  loginData={this.state.loginData} VehicleNo={this.state.itemId} Hours={'24'}  />
            }
        </View>
         {/* <View style={{ flex: 1,flexDirection:'column' }}> */}
        
         
          <CustomSubHeader  
              getHeaderTextone={headerName}
              getHeaderTexttwo={this.state.itemId}
            //  isSerIconVisible={''} 
              onPress={() => this.props.navigation.goBack()}  >
          </CustomSubHeader>

          <BottomSheet
          popupData={this.state.popupData}
          isOpen={this.state.popupIsOpen}
          onClose={this.closPopup}
          menuSelection={this.handleMenuSelection}
          />

     
          <View  style={{position: 'absolute',margin: 40,bottom:0,right:0,left:0, flexDirection: 'row',justifyContent: 'center',alignItems: 'center' }}  >
            <TouchableOpacity onPress={() => this.toggalPopup()} >
             {component}  
            </TouchableOpacity>
          </View> 
             
        {/* <View style={{ flex: 1}}> */}
         
        {/* </View> */}
      

    </Container>
    );
  }
}

export default LiveLocation;

