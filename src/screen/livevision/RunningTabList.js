
import React, { Component } from 'react'
import { Image,View,TouchableOpacity } from 'react-native';
import { Card,H3,H2 } from 'native-base';
import EasyListView from '../../component/list/EasyListView'
import styles from "./styles";
import { liveVisionTripAPI } from '../../actions/ApiAction';

const running = require("../../assets/image/location_icon_green.png");

const speed = require("../../assets/image/speed_icon.png");
const driver_name = require("../../assets/image/driver_icon.png");
const location = require("../../assets/image/area_icon.png");


class RunningTabList extends Component {


  // static defaultProps = {
  //   empty: false,
  //   error: false,
  //   noMore: false,
  //   column: 1
  // }

  constructor(props) {
    super(props)
    this.renderListItem = this._renderListItem.bind(this) 
    this.onFetch = this._onFetch.bind(this)
  }

  render() {
    return (
      <EasyListView
        ref={component => this.listview = component}
        column={1}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetch}
        emptyContent={'No record found.'}
      />
    )
  }

  _onListPress = (item) => {
    this.props.moveToLiveVisionDetail(item);    
   };

  _renderListItem(rowData, sectionID, rowID, highlightRow) {
    return (
        <View>
            <TouchableOpacity  onPress={() => this._onListPress(rowData)}  >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                    <Image  source= {running} style={styles.listIcon}/>
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                            {rowData.Vehicle}
                        </H2>
                        <H3 key={rowData.Customer}  style={{ fontSize:10}}>
                            {rowData.Customer}
                        </H3>
                    </View>
                    <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3 key={"date"}  >
                            {rowData.Date}
                        </H3>
                    </View>
                  </View>
                  <View style={styles.ItemContainer2}>
                  <View > 
                     <Image source= {speed} style={styles.otherIcon} >
                       </Image>
                       </View>
                       <View style={{paddingLeft:5}}>
                       <H3 key={rowData.Speed} >
                         {rowData.Speed}
                         </H3>
                         </View>
                         <View style={{paddingLeft:15}}>
                         <Image source= {driver_name} style={styles.otherIcon} >
                       </Image>
                       </View>
                       <View style={{paddingLeft:5}}>
                       <H3 key={rowData.DriverName} >
                         {rowData.DriverName }
                         </H3>
                         </View>
                    </View>
                    <View style={styles.listItemContainer3}>
                        <Image source= {location} style={styles.otherIcon}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3  key={rowData.Location}>{rowData.Location}</H3>
                        </View>
                    </View> 
                  </View>
                </Card>
             </TouchableOpacity>
        </View>
    )
  }

  getDataFromApi(pageNo, success, failure){
    liveVisionTripAPI({ PageNo:pageNo, vehicleStatus : 'running',loginData: this.props.loginData })
        .then((res) => {
          if (typeof res.vehicleDetails !== 'undefined' && res.vehicleDetails.length > 0) {
            success(res.vehicleDetails);
          }
        })
        .catch((error) => {
          failure('connectionError', null);
        });
  }

  _onFetch(pageNo, success, failure) {
    // if (this.props.empty) {
    //     success([]);
    //   }
    //   else if (this.props.error) {
    //     if (pageNo === 1) {
    //       this.getDataFromApi(pageNo, success, failure);
    //     }
    //     else {
    //       failure('Unable to connect to server.', null)
    //     }
    //   }
    //   else if (this.props.noMore) {
    //     if (pageNo === 1) {
    //       this.getDataFromApi(pageNo, success, failure);
    //     }
    //     else {
    //       success([])
    //     }
    //   }
    //   else {
        this.getDataFromApi(pageNo, success, failure);
  //    }
  }
}

export default RunningTabList;