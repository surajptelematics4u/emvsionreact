import React, { Component } from 'react';
import {  StatusBar } from "react-native";
import { Container, Tab, Tabs } from 'native-base';
import CustomHeader from '../../component/CustomHeader';


import Tab1 from './AllTabList';
import Tab2 from './IdleTabList';
import Tab3 from './StoppageTabList';
import Tab4 from './RunningTabList';

export default class LiveVision extends Component {

  constructor(props) {
    super(props);
    const { navigation } = this.props;

    this.state = {
      loginData : navigation.getParam('loginData', 'NO-ID'),
      isComponentNotActive: false
    }
  }

  componentDidMount() {
  }

  handlemoveToLiveVisionDetail= (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('LiveLocation', {
      itemId: rowData.Vehicle,
      otherParam: rowData,
      loginData:this.state.loginData
    });
  }

  handleonSearchPress=() => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('LiveVisionSearch', {
      tagName: 'Vehicle Search',
      loginData: this.state.loginData
    });
  }


  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />  
        <CustomHeader  
        getHeaderText={'Vehicle Dashboard'} 
        isSerIconVisible={true} 
        isRefIconVisible={false}
        onPress={() => this.props.navigation.openDrawer()} 
        onSearchPress={() => this.handleonSearchPress()} 
        />
        <Tabs >
          <Tab heading="All">
            <Tab1  
            moveToLiveVisionDetail={this.handlemoveToLiveVisionDetail} 
            loginData={this.state.loginData}
            isComponentNotActive={this.state.isComponentNotActive} 
            />
          </Tab>
          <Tab heading="Idle">
            <Tab2  
            moveToLiveVisionDetail={this.handlemoveToLiveVisionDetail} 
            loginData={this.state.loginData}
            isComponentNotActive={this.state.isComponentNotActive} 
            />
          </Tab>
          <Tab heading="Stoppage">
            <Tab3 
            moveToLiveVisionDetail={this.handlemoveToLiveVisionDetail} 
            loginData={this.state.loginData}
            isComponentNotActive={this.state.isComponentNotActive}  
            />
          </Tab>
          <Tab heading="Running">
            <Tab4 
            moveToLiveVisionDetail={this.handlemoveToLiveVisionDetail} 
            loginData={this.state.loginData}
            isComponentNotActive={this.state.isComponentNotActive} 
            />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
