import React, { Component } from 'react';
import { Image,View,TouchableOpacity,ListView,StatusBar } from 'react-native';
import { Container,Content,Fab,H2,Toast,Card,H3 } from 'native-base';

import CustomHeader from '../../component/CustomHeader';
import SearchBar from '../../component/SearchBar';
import colors from '../../theme/color';
import EasyListView from '../../component/list/EasyListView'

import fetchData from '../../component/CancelableFetch';
import { liveVisionSearchAPI } from '../../actions/ApiAction';
import styles from "./styles";

const idle = require("../../assets/image/location_icon_blue.png");
const stoppage = require("../../assets/image/location_icon_black.png");
const running = require("../../assets/image/location_icon_green.png");

const speed = require("../../assets/image/speed_icon.png");
const driver_name = require("../../assets/image/driver_icon.png");
const location = require("../../assets/image/area_icon.png");

var ds = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 });

class LiveVisionSearch extends Component {

  constructor(props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
      mounted:false,
      isComponentNotActive: false,
      clearIconVisible: false,
      textprogressVisible: false,
      loginData : navigation.getParam('loginData', 'NO-ID'),
      tagName : navigation.getParam('tagName', 'Search'),
      textSearch: '',
      PageNo : 0,
      dataSource: ds.cloneWithRows([]),
      rawData: []
    };
  }


  componentDidMount(){

  }

  onChangeText = (text) => {
    this.setState({PageNo: 0 ,rawData: []}); 

    if(text.length > 0){
      this.setState({ clearIconVisible : true});
      if(text.length > 2){
        fetchData.abort(2);
        this.getliveVisionSearchAPI(text,0); 
      } 
    }else{
      this.onClearText();
    }
  }

  onClearText = () => {
    this.setState({ clearIconVisible : false,textSearch: ''});
    this.setState({PageNo: 0,rawData: [],dataSource: ds.cloneWithRows([])}); 
    fetchData.abort(2);
  }

  getliveVisionSearchAPI = (text,page) => {
    let newPage = page + 1;
    let loginData = this.state.loginData;
    this.setState({ textprogressVisible : true,textSearch: text,PageNo: newPage});
    liveVisionSearchAPI({ searchText: text,PageNo: newPage,loginData : loginData })
        .then((res) => {
          this.setState({ textprogressVisible : false});
          
          var newData = res.vehicleDetails instanceof Array ? res.vehicleDetails : JSON.parse(res.vehicleDetails);
          var allData = (this.state.PageNo == 1) ? newData : this.state.rawData.concat(newData);
          //if(!this.state.isComponentNotActive){
           this.setState({ rawData: allData});
           this.setState({dataSource: ds.cloneWithRows(allData)}); 
          //}
        })
        .catch((error) => {
          if(this.state.mounted) { this.setState({textprogressVisible :false}); }
          Toast.show({
            text: error+'', 
            type: "danger"
          });
        });
  }

  onEndReached = () => {  
    let text = this.state.textSearch;
    let page = this.state.PageNo;
    this.getliveVisionSearchAPI(text,page); 
  }

  render() {
    return (
      <Container>
           <StatusBar barStyle="light-content" />  
        <CustomHeader isbackVisible={true}  getHeaderText={'Vehicle Search'} isSerIconVisible={false}  onPress={() => this.props.navigation.goBack()}  />
        <View style={{height: 50}}>
          <SearchBar
              placeholder="Enter Vehicle Number"
              autoCapitalize={'characters'}
              returnKeyType={'done'}
              autoCorrect={false}
              maxLength={20}
              onClearTextValue={this.onClearText}
              onChangeTextValue={this.onChangeText}
              clearIconVisible ={this.state.clearIconVisible}
              textprogressVisible ={this.state.textprogressVisible}
              />
        </View>
          {this.state.dataSource.getRowCount() !== 0 && <ListView
             style={{flex:1}}
             dataSource={this.state.dataSource}
             renderRow={(item) => this._renderListItem(item)}
             onEndReached={this.onEndReached}
            />
          }
          {/* {this.state.dataSource.getRowCount() === 0 && <View  style={{flex:1,justifyContent: 'center',alignContent:'center'}}>
            <H2>   </H2>
          </View>
          } */}
      </Container>
    );
  }

  _onListPress = (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('LiveLocation', {
      itemId: rowData.Vehicle,
      otherParam: rowData,
      loginData:this.state.loginData
    });
   };

  _renderListItem(rowData) {
    let component = null;
    switch(rowData.VehicleStatus) {
      case 'stoppage':
        component =  <Image  source= {stoppage} style={styles.listIcon} />
        break;
      case 'running':
        component = <Image  source= {running} style={styles.listIcon}/>
        break;
      default:
        component = <Image  source= {idle} style={styles.listIcon}/>
    } 

    return (
        <View>
            <TouchableOpacity key={rowData.Date} onPress={() => this._onListPress(rowData)}  >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                       {component}
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                            {rowData.Vehicle}
                        </H2>
                        <H3 key={rowData.customerName}  style={{ fontSize:10}}>
                            {rowData.customerName}
                        </H3>
                    </View>
                    <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3 key={"date"}  >
                            {rowData.Date}
                        </H3>
                    </View>
                  </View>
                  <View style={styles.ItemContainer2}>
                  <View > 
                     <Image source= {speed} style={styles.otherIcon} >
                       </Image>
                       </View>
                       <View style={{paddingLeft:5}}>
                       <H3 key={rowData.Speed} >
                         {rowData.Speed}
                         </H3>
                         </View>
                         <View style={{paddingLeft:15}}>
                         <Image source= {driver_name} style={styles.otherIcon} >
                       </Image>
                       </View>
                       <View style={{paddingLeft:5}}>
                       <H3 key={rowData.driverName} >
                         {rowData.driverName }
                         </H3>
                         </View>
                    </View>
                    <View style={styles.listItemContainer3}>
                        <Image source= {location} style={styles.otherIcon}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3  key={rowData.Location}>{rowData.Location}</H3>
                        </View>
                    </View> 
                  </View>
                </Card>
             </TouchableOpacity>
        </View>
    )
  }
}

export default LiveVisionSearch;
