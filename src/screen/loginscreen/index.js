import React, { Component } from "react"; 
import { StackActions, NavigationActions } from 'react-navigation';
import * as Animatable from 'react-native-animatable';
import {  StatusBar,Image,AsyncStorage,KeyboardAvoidingView,TouchableOpacity,View,Linking } from "react-native";
import { Container,H3,H1,Content,Card,CardItem,Form,Toast } from "native-base";
import FloatLabelTextInput from "../../component/FloatingLabel";
import LoadingButton from "../../component/LoadingButton";
import fetchData from '../../component/CancelableFetch';

import baseStyles from "../../theme/styles";
import styles from "./styles";
import { loginData,userName,drawerNavigation,FeatureMenu,RESTPASSWORDURI} from "../../util/Constant";
import colors from "../../theme/color";
import { onSignInFromAPI } from '../../actions/Authentication';

const launchscreenLogo = require("../../assets/image/launcher_logo.png");

class LoginScreen extends Component { 
  
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loginData:{}
    }
 }
 
 componentWillUnmount () { 
   fetchData.abort(3);
 }

 moveToDashBoard(mloginData){
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: 'Drawer' , params: {
        loginData: mloginData
      }})],
    });
    this.props.navigation.dispatch(resetAction);
  //  this.props.navigation.replace('Drawer');
  }

 _usernameChanged = (username) => {
    this.setState({ username });
  }

  _passwordChanged = (password) => {
      this.setState({ password });
  }

  _onButtonPress = () => {
    const { username, password } = this.state; 
    if( username ) {
      if( password ) {
        this.loadingButton.showLoading(true);
        onSignInFromAPI({ username, password })
        .then((res) => {
          console.log(res);
          this.loadingButton.showLoading(false);
          if (res != null || res !== undefined) {
            if(res.status == 1){
               this.setState({loginData : {loginStatus : true,
                domainName: res.domainName,
                customerid: res.customerid,
                systemId: res.systemId,
                userId: res.userId,
                userIdAsInt: res.userIdAsInt
              }});
               AsyncStorage.setItem(loginData, JSON.stringify(this.state.loginData));
               AsyncStorage.setItem(userName, JSON.stringify(res.firstName+" "+res.lastName));
               AsyncStorage.setItem(drawerNavigation, JSON.stringify(res.Drawer));
               AsyncStorage.setItem(FeatureMenu, JSON.stringify(res.buttonDetails));
      
               this.moveToDashBoard(this.state.loginData);
      
              // this.props.navigation.replace(SLADashboardDraw);
            }else if(res.status == 0){
              Toast.show({
                text: res.message, 
                type: "danger"
              });
            }
           }
        })
        .catch((error) => {
          this.loadingButton.showLoading(false);
          Toast.show({ text: error+'' ,
          type: "warning"
        });
        });
      }else{
        Toast.show({ text: "Password is required." ,
          type: "warning"
        });
      }
    }else{
      Toast.show({ text: "Username is required." ,
        type: "warning"
      });
    }
  }
   
  render() { 
    return (
      <Container>
        <StatusBar barStyle="light-content" />  
        <KeyboardAvoidingView style={baseStyles.avoidingKeyboard} behavior="padding"   scrollEnabled={false} enabled>    
          <Content> 
            <View  style={styles.lightcontainer}>
            </View> 
              <Content > 
              <View style={[baseStyles.centerParent,styles.colourcontainer]}>
                {/* <Transition shared='logo'>   */}
                  <Image source= {launchscreenLogo} style={styles.logo} ></Image>  
                {/* </Transition> */}
                <Animatable.Text animation="pulse" easing="ease-out" iterationCount="infinite" style={{marginTop : 15,color:colors.white,fontSize:20,fontWeight: 'bold'}}>Em-Vision</Animatable.Text>
              </View>
              </Content > 
              {/* <Transition> */}
            <Card style={[baseStyles.horizontalCenter,styles.cardwidth]}> 
            <CardItem >
            <View  style = {{flex : 1,padding: 5}}>  
            <View   style = {styles.loginbox}>
            <H1 style={{color:colors.primaryColour,fontWeight: 'bold',marginBottom:25 }}>Log In</H1>
            </View>
            <Form style={{  marginTop: 5 ,marginBottom: 5,alignItems : 'center' }}>
                
                  <FloatLabelTextInput 
                      placeholder={"Username"}
                      value={this.state.username}
                      onChangeTextValue={this._usernameChanged}
                      maxLength = {25}
                      returnKeyType = { "next" }
                      onSubmitEditing={() => { this.Password.focus(); }}
                    />
                    <FloatLabelTextInput
                      placeholder={"Password"}
                      ref={(input) => { this.Password = input; }}
                      maxLength = {25}
                      secureTextEntry = {true}

                      value={this.state.password}
                      onChangeTextValue={this._passwordChanged}
                    />
                   <LoadingButton 
                             animation="bounceInUp"
                             contentInsetAdjustmentBehavior="automatic"
                             duration={1100}
                             delay={1400}
                             ref={c => (this.loadingButton = c)}
                             width={240}
                             height={30}
                             title="SUBMIT"
                             titleFontSize={15}
                             titleColor="rgb(255,255,255)"
                             backgroundColor={colors.primaryColour}
                             borderRadius={4}
                             onPress={this._onButtonPress.bind(this)}
                   />
                </Form>             
                </View>
            </CardItem>
            <CardItem footer  >
              <TouchableOpacity onPress={() => Linking.openURL(RESTPASSWORDURI)} >
                <H3  style={{color:colors.primaryColour}}>Reset Password ?</H3>
              </TouchableOpacity>
            </CardItem> 
          </Card>
          {/* </Transition> */}
             <View  style={styles.lightcontainer}>
          </View> 
          </Content> 
           </KeyboardAvoidingView>
      </Container>
    );
  }
}
 
export default LoginScreen;
