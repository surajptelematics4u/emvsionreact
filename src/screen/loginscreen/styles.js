import React,{ StyleSheet } from 'react-native';
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const devicewidth = Dimensions.get("window").width;
import color from "../../theme/color"
 

const styles = StyleSheet.create({
  
  lightcontainer: {
   // backgroundColor: color.white,
    height: deviceHeight / 10
  },
  colourcontainer: {
    backgroundColor: color.primaryColour,
    height: deviceHeight / 2.5
  },
  cardwidth: {
    marginTop:-30,
    marginLeft: devicewidth / 9,
    marginRight: devicewidth / 9,
  },
  mt2: {
    marginTop:  deviceHeight / 8
  },
  loginbox:{
  //  backgroundColor: color.lightgray,
    flex: 1,
    alignItems: "center"
  },
  logo: {
    height:100,
    width:100, 
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  }
});
export default styles;