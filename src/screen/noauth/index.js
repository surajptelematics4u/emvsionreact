import React, { Component } from 'react';
import { Image,View,StatusBar,AsyncStorage } from 'react-native';
import { Icon,Text,H2,Container,Button } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';

import colors from "../../theme/color"
import CustomHeader from '../../component/CustomHeader';
import { loginData } from "../../util/Constant"

class NoAuth extends Component {

    
    moveToLogout = () => {
        AsyncStorage.setItem(loginData, null);
        const resetAction = StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate({ routeName: 'Login'})],
        });
        this.props.navigation.dispatch(resetAction);
    }


  render() {
    return (
        <Container>
           <StatusBar barStyle="light-content" />  
        <CustomHeader  getHeaderText={'Request Feature'} isSerIconVisible={false}  onPress={() => this.props.navigation.openDrawer()}  />
        <View style={{flex: 1,justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}} >
            <H2 style={{margin : 20,textAlign: 'center'}}> Feature is not available for your account, please login. </H2>
            <Button full rounded  danger style={{margin :10}}  onPress={() =>  this.moveToLogout() } >
               <Icon name="log-out" style={{ color: colors.white }} />
                <Text>Log Out</Text>
              </Button>
        </View>
      </Container>
    );
  }
}

export default NoAuth;
