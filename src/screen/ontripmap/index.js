import React, { Component } from 'react'
import {StyleSheet,Dimensions, Image,View,TouchableOpacity,ImageBackground,ActivityIndicator } from 'react-native';
import { ListItem,Button,Container,H1,Fab,H2,Icon,H3,CheckBox,Text } from 'native-base';
import { Marker, Callout,PROVIDER_GOOGLE } from 'react-native-maps'
import ClusteredMapView from 'react-native-maps-super-cluster'
import { mapStylejson } from "../../theme/mapstyles";
import { openTripMAPAPI } from '../../actions/ApiAction';
import styles from "./styles";
import fetchData from '../../component/CancelableFetch';

import  Dialog  from '../../component/dialogs/Dialog';
import CustomSubHeader from '../../component/CustomSubbHeader';
import colors from '../../theme/color'; 

const DEFAULT_PADDING = { top: 150, right: 150, bottom: 150, left: 150 };
import { LATITUDE,LONGITUDE,LATITUDE_DELTA,LONGITUDE_DELTA,LATITUDE_DELTASUB,LONGITUDE_DELTASUB } from "../../util/MapUtil";
const clusterimage = require("../../assets/image/clusterimage.png");
const currentlocation = require("../../assets/image/current_location.png");
const location = require("../../assets/image/area_icon.png");

class OnTripMap extends Component {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
        nodatadialogVisible: false,
        isMarkerDetail : false,
        pins: [],
        selectedPin : { vehicleNumber: '',customer:'',lastCommunicationTime:'',currentlocation: '' },
        loadingDataActive: false,
        refreshing: false,
        PageNo : 0,
        itemId : navigation.getParam('itemId', 'NO-ID'),
        loginData :navigation.getParam('loginData', '')
    };
   this.renderMarker = this.renderMarker.bind(this)
   this.renderCluster = this.renderCluster.bind(this)
  }

  componentDidMount(){
   this.getopenTripMAPAPI();  
  }

  componentWillUnmount () {
    fetchData.abort(11);
  }

  getopenTripMAPAPI = () => {
    let newPage = this.state.PageNo + 1;
    this.setState({PageNo : newPage});
    this.setState({refreshing :true,loadingDataActive: true});
    openTripMAPAPI({PageNo: newPage, loginData : this.state.loginData })
        .then((res) => {
          this.setState({refreshing :false,loadingDataActive: false});
          //const pins =  res.ClusterDetails;
           if (typeof res.ClusterDetails !== 'undefined' && res.ClusterDetails.length > 0){
               if(this.state.PageNo == 1){
                this.setState({ pins: res.ClusterDetails});

                this.fitBottomTwoMarkers();
               }else{
               this.setState({ pins: this.state.pins.concat(res.ClusterDetails) })
               }
            }else{
              if(this.state.PageNo == 1){
                this.setState({nodatadialogVisible: true});
              }else{
                this.setState({loadingDataActive: true});
              }
            }
        })
        .catch((error) => {
           this.setState({refreshing :false,loadingDataActive: false});
        //  failure('Unable to connect to server.', null);
        });
  }

  renderCluster = (cluster, onPress) => {
    const pointCount = cluster.pointCount,
          coordinate = cluster.coordinate,
          clusterId = cluster.clusterId
    return (
      <Marker identifier={`cluster-${clusterId}`} coordinate={coordinate}  onPress={onPress}>
        <View style={styles.clusterContainer}>
          <Text style={styles.clusterText}>
            {pointCount}
          </Text>
        </View>
      </Marker>
    )
  }

  renderMarker = (pin) => {
    return (
      <Marker 
      image={currentlocation}
      identifier={pin.vehicleNumber}
      key={pin.tripId}
      coordinate={pin.location}
      onPress={() => this.markerclick(pin)}    
      >
      {/* <ImageBackground source={currentlocation} style={{ width: 30, height: 30 }} /> */}
     </Marker>
    )
  }

  markerclick = (pin) => {
    this.setState({selectedPin: pin,isMarkerDetail :true});
    
    let mapInstant = this.map.getMapRef();
    mapInstant.fitToCoordinates([{latitude:  pin.location.latitude,
      longitude:  pin.location.longitude}], {
      edgePadding: DEFAULT_PADDING,
      animated: true,
    });
  }

  fitBottomTwoMarkers = () => {
    if(this.state.pins.length > 0){
      let mapInstant = this.map.getMapRef();
      mapInstant.animateToRegion({
        latitude: this.state.pins[this.state.pins.length -1 ].location.latitude,
        longitude: this.state.pins[this.state.pins.length -1 ].location.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }, 1000); 
    }
  }

  moveToTripDetails = () => {
    if (typeof this.state.selectedPin.tripId !== 'undefined'){

      this.props.navigation.navigate('TripDetails', {
        itemId: this.state.selectedPin.tripId,
        itemIdVec: this.state.selectedPin.vehicleNumber,
        otherParam: {
          VehicleNo: this.state.selectedPin.vehicleNumber,
          StartDate: this.state.selectedPin.startDate,
          EndDate: this.state.selectedPin.endDate,
          TripID: this.state.selectedPin.tripId
        },
        loginData: this.state.loginData,
        tripType: 'openTrip'
      });
    }
  }

  render() {
    return (
    <Container>
        <ClusteredMapView
          ref={(r) => { this.map = r }}
          provider={PROVIDER_GOOGLE}
          style={{...StyleSheet.absoluteFillObject}}
          data={this.state.pins}
          renderMarker={this.renderMarker}
          renderCluster={this.renderCluster}
          customMapStyle={mapStylejson}
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}>
        </ClusteredMapView>
        <CustomSubHeader  
            getHeaderTextone={'On Trip'} 
            isSerIconVisible={false}
            onPress={() => this.props.navigation.goBack()}  >
        </CustomSubHeader>
       {this.state.isMarkerDetail && <View style={{backgroundColor: colors.white  , position: 'absolute',bottom:0,right:0,left:0, justifyContent: 'flex-end' }}>
                <View style={{  flex: 1, flexDirection: 'column',padding: 15}}>
                  <TouchableOpacity onPress={() => this.setState({isMarkerDetail: false})} style={{flexDirection: 'row',justifyContent: 'space-between'}} >
                     <View style={{flex:1,justifyContent: 'center'}}>
                          <H2 style={{fontWeight: "bold"}}>
                           {this.state.selectedPin.vehicleNumber}                            
                           </H2>
                      </View>
                      <View  style={{justifyContent: 'center'}} >
                          <Icon name="md-close" style={{ color: colors.gray600}} />
                      </View>
                  </TouchableOpacity>
                  <View style={{flex:1,flexDirection: 'row',justifyContent: 'flex-end'}}>
                      <H3>
                         {this.state.selectedPin.lastCommunicationTime}
                      </H3>
                  </View>  
                  <H2> 
                    {this.state.selectedPin.customer}
                  </H2>
                  <View style={{flex: 1, flexDirection: 'row',marginTop:15}}>
                      <Image source= {location} style={{height:15,width:15}}>
                      </Image>
                      <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3>{this.state.selectedPin.currentlocation}</H3> 
                      </View>
                  </View>
                  <TouchableOpacity  onPress={() => this.moveToTripDetails()} style={{flexDirection: 'row',justifyContent: 'flex-start',marginTop: 15,marginBottom: 40}} >
                        <View style={{backgroundColor: colors.primaryColour, borderRadius: 20, paddingVertical: 6, paddingHorizontal: 12}}>
                        <H2 style={{color: colors.white,fontWeight: '200'}} >
                          Trip Details
                        </H2> 
                        </View>
                  </TouchableOpacity>
                </View>
              </View>
        }
        {!this.state.loadingDataActive &&
           <Fab
              active={this.state.active}
              containerStyle={{ marginTop: 70}}
              style={{ backgroundColor: colors.white }}
              position="topRight"
              onPress={() => this.getopenTripMAPAPI() }>  
              <Icon name='download' style={{color: colors.primaryColour}} />
            </Fab>
        }
        <Fab
              active={this.state.active}
              style={{ backgroundColor: colors.white }}
              position="bottomRight"
              onPress={() => this.fitBottomTwoMarkers()}>
              <Icon name='md-locate' style={{color: colors.black}} />
        </Fab>
        <Dialog 
          visible={this.state.nodatadialogVisible} 
          >
          <View>
            <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
              <View  style={{padding:10}}>
              <H2 style={{fontWeight: '100',textAlign: 'center'}} >
              No Records Available
              </H2> 
              </View>
              <Button full rounded  primary style={{margin :10}}    onPress={() => this.props.navigation.goBack()} >
                <Text style={{color: colors.white}} >CLOSE</Text>
              </Button>
            </View>
          </View>
        </Dialog>  
        {this.state.refreshing && <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}} >
           <ActivityIndicator size="large" color={colors.primaryColour} />
            </View>
        }
    </Container>
    );
  }
}

export default OnTripMap;

