import React,{ StyleSheet } from 'react-native';
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const devicewidth = Dimensions.get("window").width;
import colors from "../../theme/color"


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  clusterContainer: {
    width: 40,
    height: 40,
    padding: 6,
    borderWidth: 1,
    borderRadius: 15,
    alignItems: 'center',
    borderColor: colors.primaryColour,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  clusterText: {
    fontSize: 13,
    color: colors.white,
    fontWeight: '500',
    textAlign: 'center',
  },
  controlBar: {
    top: 24,
    left: 25,
    right: 25,
    height: 40,
    borderRadius: 20,
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: 'white',
    justifyContent: 'space-between', 
  },
  button: {
    paddingVertical: 8,
    paddingHorizontal: 20,
  },
  novaLabLogo: {
    right: 8,
    bottom: 8,
    width: 64,
    height: 64,
    position: 'absolute',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  clusterContainer: {
    width: 34,
    height: 34,
    borderWidth: 1,
    borderRadius: 22,
    alignItems: 'center',
    borderColor: colors.white,
    justifyContent: 'center',
    backgroundColor: colors.primaryColour,
  },
  counterText: {
    fontSize: 14,
    color: colors.white,
    fontWeight: '400'
  },
  calloutStyle: {
    width: 64,
    height: 64,
    padding: 8,
    borderRadius: 8,
    borderColor: colors.white,
    backgroundColor: 'white', 
  },
})
export default styles;