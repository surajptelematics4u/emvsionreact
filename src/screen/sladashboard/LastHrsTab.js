import React, { Component, PropTypes } from 'react'
import {  View, TouchableOpacity, FlatList,RefreshControl,Image,Dimensions,ActivityIndicator } from 'react-native'
import ProgressBarAnimated from '../../component/AnimatedProgressBar';
import { Button,Text,Container,H3,H1,H2,Content,Card} from "native-base";
import styles from './styles';
import colors from '../../theme/color'; 
import fetchData from '../../component/CancelableFetch';
import  Dialog  from '../../component/dialogs/Dialog';
import LastHrsWindow from './LastHrsWindow';
import { tripAlertsAPI } from '../../actions/ApiAction';

const MAPICON = require("../../assets/image/map_icon.png");

const PanicAlert = require("../../assets/image/PanicAlert.png");
const GPSTampering = require("../../assets/image/GPSTampering.png");
const HarshBrake = require("../../assets/image/HarshBrake.png");
const Overspeed = require("../../assets/image/Overspeed.png");
const RestrictiveHours = require("../../assets/image/RestrictiveHours.png");
const EngineErrorCode = require("../../assets/image/EngineErrorCode.png");

class LastHrsTab extends Component {
 
      constructor(props) {
        super(props)
         this.state = { 
            vehiclesOnTripTotal : 0,
            vehiclesOnTripCount :0,
            gridData : [],
            refreshing: false,
            progressData : 1,
            popupIsOpen: false,
            popupData: [],
            startprogress:true,
            dialogVisible: false,
            dialogVisiblemasg: 'Unable to connect to server.'
        }
  
      }

      componentWillMount(){
        this.getDataFromApi();
      }
 
      componentWillUnmount () { 
        fetchData.abort(4);
      }

      componentWillReceiveProps(nextProps) {
        if(this.props.onTripRefresh){
          this._onRefresh();
        }
      }

      _onRefresh = () => {
        this.getDataFromApi();
      }

      getDataFromApi = () => {
        let loginData = this.props.loginData;
        this.setState({refreshing: true});
        tripAlertsAPI({loginData})
            .then((res) => {
              this.setState({gridData :res.MandatoryAlertDetails});
              this.setState({vehiclesOnTripTotal :res.total});
              this.setState({vehiclesOnTripCount :res.vehiclesontrip});
              this.setState({progressData : (parseFloat(res.vehiclesontrip)/parseFloat(res.total))*100});
              this.setState({popupData : res.Tripdetails});
              this.setState({refreshing: false,startprogress :false});
            })
            .catch((error) => { 
              this.setState({refreshing: false,startprogress :false});
              this.setState({dialogVisible: true,dialogVisiblemasg :error.toString()});
            });
      }

      openPopup = () => {
        if(this.state.popupData.length > 0 && this.state.vehiclesOnTripCount > 0 ){
          this.setState({
            popupIsOpen: true,
        //   popupData,	
          });
        }
      }
      closPopup = () => {
        this.setState({
          popupIsOpen: false,
        });
      }

      handlegoTooTripList= (tagName,tagselected) => {
        this.props.goTooTripList(tagName,tagselected);    
      }

      render() {
        const barWidth = Dimensions.get('screen').width - 40;
        const footerHeight = Dimensions.get('screen').height/2;

        return (
             <Container>
            <Content style={{flex:1,flexDirection:'column'}}>
               <View style={{alignItems : 'center'}}>
               <FlatList
                  style={{flex:1}}
                  data={this.state.gridData}
                  extraData={this.state}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => String(index)}
                  numColumns={3}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this._onRefresh}
                    />
                  }
                />
                </View>
               {!this.state.startprogress && <View  >
                <Card style={{flex : 1,marginRight : 10,marginLeft : 10,marginBottom : 10}}>
                 <View style={{flex : 1,flexDirection: 'column',padding : 10}} >
                    <View style={{flex : 1,flexDirection: 'row',justifyContent: 'space-between'}}>
                      <View  style={{justifyContent: 'flex-end',alignItems: "flex-start"}}>
                        <H3 style={{color : colors.gray600,fontWeight:'200'}} >
                        Vehicles on Trip
                        </H3>
                      </View>
                      <View style={{alignItems: "flex-end",justifyContent: 'flex-end',flexDirection: 'row'}}>
                        <H1 style={{color : colors.primaryColour,fontSize : 18,fontWeight:'bold'}} >
                          {this.state.vehiclesOnTripCount}
                        </H1>
                        <H3 style={{color : colors.gray600,fontWeight:'200'}} >
                          /{this.state.vehiclesOnTripTotal}
                          </H3>
                      </View>
                    </View>
                    <View style={{flex : 1,paddingTop: 5}} >
                        <ProgressBarAnimated
                          width={barWidth}
                          height={10}
                          value={this.state.progressData}
                          backgroundColor={'#148cF0'}
                        />
                    </View>
                    <View style={{alignItems: "flex-end",justifyContent: 'flex-end',flexDirection: 'row',marginTop:10}}>
                      <TouchableOpacity  onPress={() =>  this.openPopup() } >
                         <H3 style={{color : colors.primarylightColour,fontWeight:'200',textDecorationLine: 'underline'}} >
                           On Trip More Information
                          </H3>
                      </TouchableOpacity>
                    </View>
                  </View>
                </Card>
                </View>
               }

                {this.state.startprogress && <View style={{width: null,height:footerHeight,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
              }
            </Content>

            <LastHrsWindow
              popupData={this.state.popupData}
              isOpen={this.state.popupIsOpen}
              onClose={this.closPopup}
              goTooTripList={this.handlegoTooTripList}
            />

           {!this.state.popupIsOpen && <View style={{flexDirection: 'column'}} >
              <Button full rounded  primary style={{margin :10}}   onPress={() =>  this.props.goToOnTripMap() }   >
                <Text>On Trip Vehicle on Map</Text>
              </Button>
            </View>
            }

            <Dialog 
            visible={this.state.dialogVisible} 
            >
            <View>
              <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
                <View  style={{padding:10}}>
                <H2 style={{fontWeight: '100',textAlign: 'center'}} >
                {this.state.dialogVisiblemasg}
                </H2> 
                </View>
                <Button full rounded  primary style={{margin :10}}    onPress={() => this.setState({dialogVisible: false})} >
                  <Text style={{color: '#fff'}} >OK</Text>
                </Button>
              </View>
            </View>
          </Dialog>  
          </Container>
        )
      }
      _onListPress = (item) => {
          if(item.count !== '0/0'){
            this.props.moveToAlertDetail(item);    
          }
       };

      _renderItem = ({ item,index }) => {
       
        let component = null;
        switch(item.alertName) {
          case "Panic Alert":
            component =  <Image  source= {PanicAlert} style={styles.gridImage} />
            break;
          case "Door Sensor":
            component = <Image  source= {HarshBrake} style={styles.gridImage}/>
            break;
          case "Not Communicating":
            component = <Image  source= {GPSTampering} style={styles.gridImage}/>
            break;
          case "Humidity Alert":
            component = <Image  source= {RestrictiveHours} style={styles.gridImage}/>
            break;
          case "ReeferOff Alert":
            component = <Image  source= {EngineErrorCode} style={styles.gridImage}/>
            break;
          case "Temp Alert":
            component = <Image  source= {Overspeed} style={styles.gridImage}/>
            break;
          default:
            component = <Image  source= {PanicAlert} style={styles.gridImage}/>
        }

        return (
            <TouchableOpacity id={item.alertId}  onPress={() => this._onListPress(item)}   >
              <Card style={styles.item}>
                   <View style={{ position: 'absolute',top: 10,left: 0,right: 10,bottom: 0,justifyContent:'flex-start',alignItems:'flex-end'}}>
                       {component}
                    </View>
                <View style={{flex :1,justifyContent:'center',alignItems:'flex-start',paddingRight :10,paddingLeft:10,marginBottom:10,marginTop:30}}>
                <H1 style={{color : colors.gray700,fontSize : 16,fontWeight:'bold'}} >
                  {item.count}
                </H1>
                <H3 style={{color : colors.gray600,fontWeight:'200'}} >
                  {item.alertName}
                </H3>
                </View>
              </Card>
            </TouchableOpacity>
        )
      }
    }

export default LastHrsTab;
