import React, { Component } from 'react';
import {TouchableOpacity, Animated,Image, Dimensions,
  StyleSheet,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import { Button,Text,Container,H3,H1,H2,Content,Icon,Input,Left,Card,Body,CardItem,Form,Item,Toast } from "native-base";

import ProgressBarAnimated from '../../component/AnimatedProgressBar';

import colors from '../../theme/color'; 
const { width, height } = Dimensions.get('window');
const defaultHeight = height * 0.77;

const TIMERED = require("../../assets/image/time_red.png");
const TIMEORG = require("../../assets/image/time_org.png");
const TIMEGRE = require("../../assets/image/time_green.png");

export default class LastHrsWindow extends Component {

  state = {
    position: new Animated.Value(this.props.isOpen ? 0 : defaultHeight),
    // height: height / 2,
    visible: this.props.isOpen,
    selectedcard :1,
    selectedcardtext: 'On Time'
  };

  componentWillReceiveProps(nextProps) {
    // isOpen prop changed to true from false
    if (!this.props.isOpen && nextProps.isOpen) {
      this.animateOpen();
    }
    // isOpen prop changed to false from true
    else if (this.props.isOpen && !nextProps.isOpen) {
      this.animateClose();
    }
  }

  // Open popup
  animateOpen() {
    this.setState({ visible: true }, () => {
      Animated.timing(
        this.state.position, { toValue: 0 }   
      ).start();
    });
  }

  // Close popup
  animateClose() {
    Animated.timing(
      this.state.position, { toValue: defaultHeight }
    ).start(() => this.setState({ visible: false }));
  }

  _onCardPress = (cardtext,position) => {
    this.setState({
      selectedcard: position,
      selectedcardtext : cardtext,
    });    
   };

   goTooTripList(tagName,tagselected){
     console.log(tagName);
    this.props.goTooTripList(tagName,tagselected);    
   }

   movetoOpenTripDetail = (tagName,tagselected,tabposition) => {
      if(tabposition == 200){
        this.goTooTripList(tagName,tagselected);
      }else{
        if(tabposition == 100){
          if(tagselected === '1'){
            switch(this.state.selectedcard) {
              case 1:
              this.goTooTripList(tagName,'onTimeId-stoppage');
                break;
              case 2:
              this.goTooTripList(tagName,'delayedless1-stoppage');
                break;
              default:
              this.goTooTripList(tagName,'delayedgreater1-stoppage');
            }
          }else if(tagselected === '2'){
            switch(this.state.selectedcard) {
              case 1:
              this.goTooTripList(tagName,'onTimeId-hubhetention');
                break;
              case 2:
              this.goTooTripList(tagName,'delayedless1-detention');
                break;
              default:
              this.goTooTripList(tagName,'delayedgreater1-detention');
            }
          }else{
            switch(this.state.selectedcard) {
              case 1:
              this.goTooTripList(tagName,'onTimeId-deviation');
                break;
              case 2:
              this.goTooTripList(tagName,'delayedless1-deviation');
                break;
              default:
              this.goTooTripList(tagName,'delayedgreater1-deviation');
            }
          }
        }else{
          if(this.state.selectedcard == tabposition){
            this.goTooTripList(tagName,tagselected);
          }else{
            this.setState({
              selectedcard: tabposition,
              selectedcardtext : tagName,
            });
          }
        }
      }
   };

  render() {
    if (!this.state.visible) {
      return null;
    }
    const barWidth = Dimensions.get('screen').width - 40;
    const { popupData } = this.props;

      let firsttext ='Unplanned Stoppage';
      let firstdata = null;
      let sectext = 'SmartHub Detention';
      let sectdata = null;
      let thirdtext = 'Unplanned Deviation';
      let thirddata = null;
        switch(this.state.selectedcard) {
          case 1:
           firstdata = popupData[6].count;
           sectdata = popupData[7].count;
           thirddata = popupData[8].count;
            break;
          case 2:
           firstdata = popupData[0].count;
           sectdata = popupData[1].count;
           thirddata = popupData[2].count;
            break;
          case 3:
           firstdata = popupData[3].count;
           sectdata = popupData[4].count;
           thirddata = popupData[5].count;
            break;
          default:
           firstdata = '';
           sectdata = '';
           thirddata = '';
        }
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this.props.onClose}>
          <Animated.View style={styles.backdrop}/>
        </TouchableWithoutFeedback>
        <Animated.View
          style={[styles.modal, {
            transform: [{ translateY: this.state.position }, { translateX: 0 }]
          }]}
        >
        <Container>
            <TouchableOpacity onPress={this.props.onClose} style={{flexDirection: 'row',justifyContent: 'flex-end',padding: 15}} >
                {/* <View style={styles.buttonContainer}>
                <H2 style={{color: colors.white,fontWeight: '200'}} >
                    Vehicles on Trip
                </H2>
                </View> */}
                <View  style={{justifyContent: 'center'}} >
                    <Icon name="md-close" style={{ color: colors.gray600}} />
                </View>
            </TouchableOpacity>
          <Content>
                <Card style={{flex : 1,marginRight : 10,marginLeft : 10,marginBottom : 10}}>
                 <View style={{flex : 1,flexDirection: 'column',padding : 10}} >
                    <View style={{flex : 1,flexDirection: 'row',justifyContent: 'space-between'}}>
                      <View  style={{justifyContent: 'flex-end',alignItems: "flex-start"}}>
                      <TouchableOpacity  onPress={() => this.movetoOpenTripDetail('Enroute Placement','enrouteId',200)}>  
                        <H3 style={{color : colors.black,fontWeight:'200'}} >
                        Enroute Placement : {popupData[9].count}
                        </H3>  
                        </TouchableOpacity>
                      </View>
                      <View style={{alignItems: "flex-end",justifyContent: 'flex-end',flexDirection: 'row'}}>
                        <H1 style={{color : colors.green500,fontSize : 18,fontWeight:'bold'}} >
                          {popupData[10].count}
                        </H1>
                        <H3 style={{color : colors.red500,fontWeight:'200'}} >
                          /{popupData[9].count}
                          </H3>
                      </View>
                    </View>
                    <View style={{flex : 1,paddingTop: 5}} >
                        <ProgressBarAnimated
                          width={barWidth}
                          height={10}
                          value={(popupData[10].count / popupData[9].count)*100}
                          backgroundColor={colors.green500}
                        />
                    </View>
                    <View style={{flex : 1,flexDirection: 'row',justifyContent: 'space-between'}}>
                      <View  style={{justifyContent: 'flex-end',alignItems: "flex-start"}}>
                      <TouchableOpacity  onPress={() => this.movetoOpenTripDetail('Enroute On-Time','enrouteId-Ontime',200)}>  
                        <H3 style={{color : colors.gray600,fontWeight:'200'}} >
                        On-Time : {popupData[10].count}
                        </H3>  
                        </TouchableOpacity>
                      </View>
                      <View style={{alignItems: "flex-end",justifyContent: 'flex-end',flexDirection: 'row'}}>
                      <TouchableOpacity  onPress={() => this.movetoOpenTripDetail('Enroute Delayed','enrouteId-delay',200)}>
                         <H3 style={{color : colors.gray600,fontWeight:'200'}} >
                        Delayed : {popupData[11].count}
                          </H3>
                      </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </Card>
                <View style={{flex : 1,flexDirection: 'row'}}>
                <TouchableOpacity  onPress={() => this.movetoOpenTripDetail('On Time','onTimeId',1)}   style={{flex :1,alignItems: 'center',justifyContent: 'center'}} >
                  <Card style={[styles.item,this.state.selectedcard == 1 ? {borderColor: colors.primaryColour,borderWidth: 5} : null]}>
                      <View style={{ position: 'absolute',top: 10,left: 0,right: 10,bottom: 0,justifyContent:'flex-start',alignItems:'flex-end'}}>
                      <Image  source= {TIMEGRE} style={styles.gridImage}/>
                        </View>
                    <View style={{flex :1,justifyContent:'center',alignItems:'flex-start',paddingRight :10,paddingLeft:10,marginBottom:10,marginTop:30}}>
                    <H1 style={{color : colors.green500,fontSize : 18,fontWeight:'bold'}} >
                       {popupData[12].count}
                    </H1>
                    <H3 style={{color : colors.black,fontWeight:'200'}} >
                      {'On Time'}
                    </H3>
                    </View>
                  </Card>
                </TouchableOpacity>
                <TouchableOpacity  onPress={() => this.movetoOpenTripDetail('Delayed < 1 hour','delayedless1',2)}   style={{flex :1,alignItems: 'center',justifyContent: 'center'}} >
                <Card style={[styles.item,this.state.selectedcard == 2 ? {borderColor: colors.primaryColour,borderWidth: 5} : null]}>
                      <View style={{ position: 'absolute',top: 10,left: 0,right: 10,bottom: 0,justifyContent:'flex-start',alignItems:'flex-end'}}>
                      <Image  source= {TIMEORG} style={styles.gridImage}/>
                        </View>
                    <View style={{flex :1,justifyContent:'center',alignItems:'flex-start',paddingRight :10,paddingLeft:10,marginBottom:10,marginTop:30}}>
                    <H1 style={{color : colors.yellow500,fontSize : 18,fontWeight:'bold'}} >
                     {popupData[13].count}
                    </H1>
                    <H3 style={{color : colors.black,fontWeight:'200'}} >
                      {'Delayed < 1 hour'}
                    </H3>
                    </View>
                  </Card>
                </TouchableOpacity>
                <TouchableOpacity  onPress={() => this.movetoOpenTripDetail('Delayed > 1hour','delayedgreater1',3)}   style={{flex :1,alignItems: 'center',justifyContent: 'center'}} >
                <Card style={[styles.item,this.state.selectedcard == 3 ? {borderColor: colors.primaryColour,borderWidth: 5} : null]}>
                      <View style={{ position: 'absolute',top: 10,left: 0,right: 10,bottom: 0,justifyContent:'flex-start',alignItems:'flex-end'}}>
                      <Image  source= {TIMERED} style={styles.gridImage}/>
                        </View>
                    <View style={{flex :1,justifyContent:'center',alignItems:'flex-start',paddingRight :10,paddingLeft:10,marginBottom:10,marginTop:30}}>
                    <H1 style={{color : colors.red500,fontSize : 18,fontWeight:'bold'}} >
                    {popupData[14].count}
                    </H1>
                    <H3 style={{color : colors.black,fontWeight:'200'}} >
                     {'Delayed > 1hour'}
                    </H3>
                    </View>
                  </Card>
                </TouchableOpacity>
                </View>
                <Card style={{flex : 1,flexDirection: 'column',marginLeft:10,marginRight:10}}>
                  <View style={{flex :1,alignItems: 'center',justifyContent: 'center',paddingLeft:10,paddingTop: 10,borderBottomColor: colors.verylightgray,borderBottomWidth: 1}} >
                  <H2 style={{color: colors.primaryColour,fontWeight: '200'}} >
                    {this.state.selectedcardtext}
                   </H2>
                  </View>                  
                  <View style={{flex : 1,flexDirection: 'row'}}>
                    <TouchableOpacity  onPress={() => this.movetoOpenTripDetail(this.state.selectedcardtext+' Unplanned Stoppage','1',100)} style={{flex :1,alignItems: 'center',justifyContent: 'center',padding:5}} > 
                      <H1 style={{color : colors.black,fontSize : 18,fontWeight:'bold'}} >
                        {firstdata}
                        </H1>
                        <H3 style={{flex :1,color : colors.black,fontWeight:'200',textAlign: 'center'}} >
                        {firsttext} 
                        </H3>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.movetoOpenTripDetail(this.state.selectedcardtext+' SmartHub Detention','2',100)}  style={{flex :1,alignItems: 'center',justifyContent: 'center',padding:5}} > 
                        <H1 style={{color : colors.black,fontSize : 18,fontWeight:'bold'}} >
                        {sectdata}
                        </H1>
                        <H3 style={{flex :1,color : colors.black,fontWeight:'200',textAlign: 'center'}} >
                        {sectext}
                        </H3>
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={() => this.movetoOpenTripDetail(this.state.selectedcardtext+' Unplanned Deviation','3',100)}  style={{flex :1,alignItems: 'center',justifyContent: 'center',padding:5}} > 
                      <H1 style={{color : colors.black,fontSize : 18,fontWeight:'bold'}} >
                       {thirddata}
                        </H1>
                        <H3 style={{flex :1,color : colors.black,fontWeight:'200',textAlign: 'center'}} >
                        {thirdtext}
                        </H3>
                    </TouchableOpacity>
                  </View>
                </Card>
                <View style={{flex : 1,flexDirection: 'row',flexWrap: 'wrap'}}>
                  <TouchableOpacity style={{flex :1}}  onPress={() => this.movetoOpenTripDetail('Unloading Detention','unloading-detention',200)}>
                  <Card style={{flex :1,alignItems: 'flex-start',justifyContent: 'center',marginLeft:10,marginRight:5,padding:5}}  >
                    <H1 style={{color : colors.black,fontSize : 18,fontWeight:'bold'}} >
                       {popupData[16].count}
                    </H1>
                    <H3 style={{color : colors.black,fontWeight:'200'}} >
                      {'Unloading Detention'}
                    </H3>
                  </Card>
                  </TouchableOpacity>
                  <TouchableOpacity style={{flex :1}}   onPress={() => this.movetoOpenTripDetail('Loading Detention','loading-detention',200)}>
                  <Card style={{flex :1,alignItems: 'flex-start',justifyContent: 'center',marginLeft:10,marginRight:5,padding:5}} >
                    <H1 style={{color : colors.black,fontSize : 18,fontWeight:'bold'}} >
                     {popupData[15].count}
                    </H1>
                    <H3 style={{color : colors.black,fontWeight:'200'}} >
                      {'Loading Detention'}
                    </H3>
                  </Card>
                  </TouchableOpacity>
                  <TouchableOpacity style={{flex :1}}   onPress={() => this.movetoOpenTripDetail('Delayed -Late Departure','delay-late-departure',200)}>
                  <Card style={{flex :1,alignItems: 'flex-start',justifyContent: 'center',marginLeft:10,marginRight:5,padding:5}} >
                    <H1 style={{color : colors.black,fontSize : 18,fontWeight:'bold'}} >
                    {popupData[17].count}
                    </H1>
                    <H3 style={{color : colors.black,fontWeight:'200'}} >
                     {'Delayed -Late Departure'}
                    </H3>
                  </Card>
                  </TouchableOpacity>
                </View>
          </Content>
        </Container>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,   // fill up all screen
    justifyContent: 'flex-end',         // align popup at the bottom
    backgroundColor: 'transparent',     // transparent background
  },
  backdrop: {
    ...StyleSheet.absoluteFillObject,   // fill up all screen
    backgroundColor: 'black',
    opacity: 0.5,
  },
  modal: {
    //height: height / 2,   
    height:defaultHeight,              // take half of screen height
    backgroundColor: 'white',
  },
  buttonContainer: {
    backgroundColor: colors.primaryColour,
    borderRadius: 20,
    paddingVertical: 6,
    paddingHorizontal: 12,
  },
  item: {
    flex: 1,
    height: 100,
    width : width/3.5,
    //width : 110,
    margin: 1
  },
  gridImage  :{
    height:35,
    width:35
  }
});