import React, { Component } from 'react'
import { Image,View,TouchableOpacity,Text,ActivityIndicator,Dimensions } from 'react-native';
import { Card,H3,H2,Toast } from 'native-base';
import EasyListView from '../../component/list/EasyListView';
import styles from "./styles";
import colors from "../../theme/color"
import { closeTripAPI } from '../../actions/ApiAction';
import fetchData from '../../component/CancelableFetch';

const idle = require("../../assets/image/location_icon_blue.png");

const locationstart = require("../../assets/image/location_start.png");
const locationend = require("../../assets/image/location_end.png");
const distanceicon = require("../../assets/image/distance_icon.png");


class OnTrip extends Component {

  // static defaultProps = {
  //   empty: false,
  //   error: false,
  //   noMore: false,
  //   column: 1
  // }
  

  constructor(props) {
    super(props)
    this.state = {
     // startprogress: true,
    //  shownodata: false,
      mounted:false
    }
    this.renderListItem = this._renderListItem.bind(this) 
    this.onFetch = this._onFetch.bind(this)
  }

  componentDidMount(){
    this.state.mounted = true;
  }
  componentWillUnmount () {
    this.state.mounted = false;
    fetchData.abort(5);
  }


  render() {
    const barWidth = Dimensions.get('screen').width - 40;
    const footerHeight = Dimensions.get('screen').height/2;
    
    return (
      <EasyListView
            ref={component => this.listview = component}
            column={1}
            style={{flex:1}}
            renderItem={this.renderListItem}
            refreshHandler={this.onFetch}
            loadMoreHandler={this.onFetch} 
            emptyContent={'No record found.'}
        />
    )
  }
  _onListPress = (rowData) => {
    this.props.moveToTripDetail(rowData);    
   };

  _renderListItem = (rowData, sectionID, rowID, highlightRow) => {
    let component = null;
    switch(rowData.Speed) {
      default:
        component = <Image  source= {idle} style={styles.listIcon}/>
    }

    return ( 
            <TouchableOpacity id={rowData}  onPress={() => this._onListPress(rowData)} >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                    <Image  source= {idle} style={styles.listIcon}/>
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                            {rowData.tripNo} 
                        </H2>
                        <H3 key={rowData.VehicleNo}  style={{ fontSize:10}}>
                            {rowData.VehicleNo}
                        </H3>
                    </View>
                    <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3 key={"date"}  >
                            {rowData.EndDate}
                        </H3>
                    </View>
                  </View>
                   <View>
                   <View style={{position: 'absolute',top:0,bottom:0,right:0,left:0,marginLeft:12,marginTop:25,borderLeftColor: colors.verylightgray,borderLeftWidth: 1}} />
                    <View style={[styles.listItemContainer3]}>
                        <Image source= {locationstart}  style={[styles.otherIcon,{marginTop: 10,paddingBottom:10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10,paddingBottom:10}}>
                        <H3  key={rowData.Source}>{rowData.Source}</H3>
                        </View>
                    </View>
                  </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {locationend}  style={[styles.otherIcon]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3 key={rowData.Destination}>{rowData.Destination}</H3>
                        </View>
                    </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {distanceicon} style={[styles.otherIcon,{marginTop: 10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10}}>
                        <H3  >{rowData.Distance} kms</H3>
                        </View>
                    </View>  
                  </View>
                </Card>
             </TouchableOpacity> 
    )
  }

  getDataFromApi = (pageNo, success, failure) => {
    let loginData = this.props.loginData;
    let NoofDays = this.props.NoofDays;
    closeTripAPI({ NoofDays: NoofDays, PageNo:pageNo, loginData : loginData })
        .then((res) => {
          if(this.state.mounted) {
            // if ((typeof res.TripInfo === 'undefined' || res.TripInfo.length < 1)  && this.state.startprogress) {
            //   this.setState({shownodata: true});
            // }
          //  this.setState({startprogress :false});
           // if(!this.props.isComponentNotActive){
              success(res.TripInfo);
           //  }else{
             // failure('loding fail...', null);
          //  } s
          }
        })
        .catch((error) => {
        //  if(this.state.mounted) { this.setState({shownodata: false}); }
       //   if(this.state.mounted) { this.setState({shownodata: false,startprogress :false}); }
          failure('connectionError', null);
        });
  }

  _onFetch(pageNo, success, failure) {
    // console.log(pageNo);
    //   if (this.props.empty) {
    //     success([]);
    //   }
    //   else if (this.props.error) {
    //     if (pageNo === 1) {
    //       this.getDataFromApi(pageNo, success, failure);
    //     }
    //     else {
    //       failure('Unable to connect to server.', null);
    //     }
    //   }
    //   else if (this.props.noMore) {
    //     if (pageNo === 1) {
    //       this.getDataFromApi(pageNo, success, failure);
    //     }
    //     else {
    //       success([])
    //     }
    //   }
    //   else {
        this.getDataFromApi(pageNo, success, failure);
 //     }
  }
}

export default OnTrip;
