import React, { Component } from 'react';
import { StyleSheet, FlatList, View,TouchableOpacity } from 'react-native';
import { Button,Text,Container,H3,H1,H2,Content,Icon,Input,Left,Card,Body,CardItem,Form,Item,Toast } from "native-base";

import styles from './styles';
import colors from '../../theme/color'; 

import OnTrip from './OnTrip';

const datas = [
  "On Trip",
  "Last 2 Days",
  "Last 1 Week",
  "Last 15 Days",
  "Last 1 Month"
];

class TripTab extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isSelectedTab : 1
    };
  }


  handlemoveToTripDetail= (rowData) => {
    this.props.moveToTripDetailmain(rowData); 
  }

  render() {
    var self = this;
    return (
      <Container>
       <View style={{flexDirection: 'column',height :55}} >
              <Card style={{flex : 1,flexDirection: 'row'}}>
                {/* <TouchableOpacity onPress ={() => this.setState({isSelectedTab : 1})}  style={[this.state.isSelectedTab == 1 ? styles.isSelected : styles.tabStyle]} > 
                  <H3 style={[this.state.isSelectedTab == 1 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`On Trip`}
                  </H3>
                </TouchableOpacity> */}
                <TouchableOpacity onPress ={() => this.setState({isSelectedTab : 1})}  style={[this.state.isSelectedTab == 1 ? styles.isSelected : styles.tabStyle]} > 
                  <H3 style={[this.state.isSelectedTab == 1 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`   Last\n2 Days`}
                  </H3>
                </TouchableOpacity>
                <TouchableOpacity onPress ={() => this.setState({isSelectedTab : 2})}  style={[this.state.isSelectedTab == 2 ? styles.isSelected : styles.tabStyle]} > 
                  <H3 style={[this.state.isSelectedTab == 2 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`   Last\n1 Week`}
                  </H3>
                </TouchableOpacity>
                <TouchableOpacity onPress ={() => this.setState({isSelectedTab : 3})}  style={[this.state.isSelectedTab == 3 ? styles.isSelected : styles.tabStyle]} > 
                  <H3 style={[this.state.isSelectedTab == 3 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`    Last\n15 Days`}
                  </H3>
                </TouchableOpacity>
                <TouchableOpacity  onPress ={() => this.setState({isSelectedTab : 4})} style={[this.state.isSelectedTab == 4 ? styles.isSelected : styles.tabStyle]} > 
                  <H3 style={[this.state.isSelectedTab == 4 ? {fontWeight:'300',color: colors.primaryColour} : {fontWeight:'300'}]} >
                  {`    Last\n1 Month`}
                  </H3>
                </TouchableOpacity>
              </Card>
         </View>
          {/* <Content style={{flex:1,flexDirection:'column'}}> */}
           { this.state.isSelectedTab == 1 &&
              <OnTrip  
              moveToTripDetail={this.handlemoveToTripDetail} 
              loginData={this.props.loginData} 
              NoofDays={2}
              isComponentNotActive={this.props.isComponentNotActive} />
           }
           { this.state.isSelectedTab == 2 &&
              <OnTrip  
              moveToTripDetail={this.handlemoveToTripDetail} 
              loginData={this.props.loginData} 
              NoofDays={7}
              isComponentNotActive={this.props.isComponentNotActive} />
           }
           { this.state.isSelectedTab == 3 &&
              <OnTrip  
              moveToTripDetail={this.handlemoveToTripDetail} 
              loginData={this.props.loginData} 
              NoofDays={15}
              isComponentNotActive={this.props.isComponentNotActive} />
           }
           { this.state.isSelectedTab == 4 &&
              <OnTrip  
              moveToTripDetail={this.handlemoveToTripDetail} 
              loginData={this.props.loginData} 
              NoofDays={30}
              isComponentNotActive={this.props.isComponentNotActive} />
           }
         {/* </Content> */}
      </Container>
    );
  }
}

// var styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });


export default TripTab;