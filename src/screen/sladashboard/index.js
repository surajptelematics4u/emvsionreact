import React, { Component } from 'react';
import {  StatusBar } from "react-native";
import { Container, Tab, Tabs } from 'native-base';
import CustomHeader from '../../component/CustomHeader';

import Tab1 from './LastHrsTab';
import Tab2 from './TripTab';

class SLADashboard extends Component {

  constructor(props) {
    super(props);
    const { navigation } = this.props;

    this.state = {
      loginData : navigation.getParam('loginData', 'NO-ID'),
      onTripRefresh: false,
      isComponentNotActive: false
    }
  } 
  componentDidMount() {
  }

  handlemoveToAlertDetail= (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('AlertList', {
      itemId: rowData.alertId,
      otherParam: rowData,
      loginData: this.state.loginData
    });
  }

  handlegoToOnTripMap= (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('OnTripMap', {
      itemId: 'Open Trip',
      loginData: this.state.loginData
    });
  }

  handlemoveToTripDetailmain= (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('TripDetails', {
      itemId: rowData.TripID,
      itemIdVec: rowData.VehicleNo,
      otherParam: rowData,
      loginData: this.state.loginData,
      tripType: 'closeTrip'
    });
  }

  handlegoTooTripList= (tagName,tagselected) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('TripList', {
      tagName: tagName,
      tagselected: tagselected,
      loginData: this.state.loginData
    });
  }

  handleonRefreshPress=() => {
    this.setState({onTripRefresh: true});
  }

  handleonSearchPress=() => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('TripSearch', {
      tagName: 'Trip Search',
      loginData: this.state.loginData
    });
  }

  render() {
    // <Tabs onChangeTab={({ i, ref, from })=> console.log(i)} > 
    return (
      <Container>
         <StatusBar barStyle="light-content" />  
        <CustomHeader  
        getHeaderText={'Trip Dashboard'} 
        isSerIconVisible={true}
        isRefIconVisible={true}
        onPress={() => this.props.navigation.openDrawer()} 
        onRefreshPress={() => this.handleonRefreshPress()}
        onSearchPress={() => this.handleonSearchPress()}
         />
        <Tabs>
          <Tab heading="On Trip">
            <Tab1 
            moveToAlertDetail={this.handlemoveToAlertDetail} 
            onTripRefresh={this.state.onTripRefresh} 
            loginData={this.state.loginData} 
            isComponentNotActive={this.state.isComponentNotActive} 
            goTooTripList={this.handlegoTooTripList}  
            goToOnTripMap={this.handlegoToOnTripMap} />
          </Tab>
          <Tab heading="Close Trip">
            <Tab2  
            moveToTripDetailmain={this.handlemoveToTripDetailmain} 
            loginData={this.state.loginData} 
            isComponentNotActive={this.state.isComponentNotActive} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

export default SLADashboard;
 