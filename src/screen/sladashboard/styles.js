import React,{ StyleSheet } from 'react-native';
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const devicewidth = Dimensions.get("window").width;
import colors from "../../theme/color"

const size = Dimensions.get('window').width/3;

const styles = StyleSheet.create({
    item: {
      flex: 1,
      height: 130,
      width : devicewidth/3.2,
      //width : 110,
      margin: 1
    },
    gridImage  :{
      height:35,
      width:35
    },
    tabStyle  : {
      flex : 1,
      justifyContent : 'center',
      alignItems : 'center'
   },
    isSelected: {
      flex : 1,
      justifyContent : 'center',
      alignItems : 'center',
      borderBottomColor: colors.primaryColour,
      borderBottomWidth: 1
   },
   container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    backgroundColor: '#ecf0f1',
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d7da',
    padding: 6,
  },
  imageWrapper: {
    padding: 5,
  },
  title: {
    textAlign: 'left',
    margin: 6,
  },
  subtitle: {
    textAlign: 'left',
    margin: 6,
  },
  container: {
    flex: 1,
    marginTop:20,
  },
    listIcon:{
      height:25,
      width:25
    },
    otherIcon:{
      height:15,
      width:15
    },
    listItemContainer:{
     flex: 1,
     flexDirection: 'column',
    },
    ItemContainer1:{
      flex:2,
      flexDirection: 'row',
     },
    ItemContainer2:{
      paddingLeft:5,
      paddingTop: 10,
      flex: 1,
      flexDirection: 'row'
     },
     listItemContainer3:{
      paddingLeft:5,
      flex: 1,
      flexDirection: 'row',
     },
    VehicleName:{
      fontWeight: "bold",
     },  
  TextInputStyleClass:{     
    textAlign: 'center',
    height: 40,
    borderWidth: 1,
    borderColor: '#009688',
    borderRadius: 7 ,
    backgroundColor : "#FFFFFF"
         
    }
   
});
export default styles;