import React, { Component } from "react";
import { ImageBackground,Image, StatusBar,AsyncStorage } from "react-native";
import { Container,Grid,Row,Content,H2 } from "native-base";
import * as Animatable from 'react-native-animatable';
import { NavigationActions,StackActions } from 'react-navigation';

import baseStyles from "../../theme/styles";
import styles from "./styles";
import {splash_screen_waitTime,loginData} from "../../util/Constant";
import color from "../../theme/color";

const launchscreenBg = require("../../assets/image/splash.png");
const launchscreenLogo = require("../../assets/image/launcher_logo.png");

class SplashScreen extends Component {

  moveTohome(mloginData){
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: 'Drawer' , params: {
        loginData: mloginData
      }})],
    });
     this.props.navigation.dispatch(resetAction);
  //  this.props.navigation.replace('Drawer');
  }

  moveToLogin(){
    this.props.navigation.replace('Login');
  }

  componentWillMount(){
    setTimeout(() => {
        AsyncStorage.getItem(loginData)
          .then((result) => {
            try { 
              if (result != null ) {
                let mloginData = JSON.parse(result);
                if(mloginData.loginStatus !== undefined){
                  console.log('login '+mloginData.loginStatus);
                    if (mloginData.loginStatus === true) {
                      this.moveTohome(mloginData);
                    } else {
                      this.moveToLogin();
                    }
                }else{
                  this.moveToLogin();
                }
              }else{
                this.moveToLogin();
              }
            }catch(err) {
              console.log('login err '+err);
              this.moveToLogin();
            }
         });
     },splash_screen_waitTime);
  }


  render() {
    return (
      <Container> 
         <StatusBar barStyle="light-content" />  
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
           <Grid>
            <Row size={6} >
            <Content padder>
            <Animatable.View animation="bounceIn" style={[baseStyles.horizontalCenter,styles.mt7]}>
                <Image source= {launchscreenLogo} style={styles.logo} ></Image>
                <H2 style={{color:color.white}}>PRIVATE LIMITED</H2> 
            </Animatable.View>
            </Content>
            </Row>
            <Row size={1} >
           
            </Row>
          </Grid>
        </ImageBackground>
      </Container>
    );
  }
}

export default SplashScreen;


