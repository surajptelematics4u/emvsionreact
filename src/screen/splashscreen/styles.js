import React,{ StyleSheet } from 'react-native';
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logo: {
    height:120,
    width:120, 
    shadowColor: "#000000",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  mt7: {
    marginTop:  deviceHeight / 7
  }
});

export default styles;