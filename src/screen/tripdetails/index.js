import React, { Component } from 'react'
import {StyleSheet,Dimensions, Image,View,TouchableOpacity,FlatList,ActivityIndicator,ImageBackground } from 'react-native';
import { ListItem,Button,Container,H1,Fab,H2,Icon,H3,CheckBox,Text } from 'native-base';
import MapView, { Polyline,Marker,PROVIDER_GOOGLE } from 'react-native-maps';
import fetchData from '../../component/CancelableFetch';

import CustomSubHeader from '../../component/CustomSubHeader';
import  Dialog  from '../../component/dialogs/Dialog';
import { mapStylejson } from "../../theme/mapstyles";
import colors from '../../theme/color'; 
import { tripPathFromApi,tripHistoryPathFromApi,tripAlertPointsFromApi } from '../../actions/ApiAction';

const DEFAULT_PADDING = { top: 150, right: 150, bottom: 150, left: 150 };
const { width, height } = Dimensions.get('window');

import { LATITUDE,LONGITUDE,LATITUDE_DELTA,LONGITUDE_DELTA } from "../../util/MapUtil";

const startmark = require("../../assets/image/marker_location_pick.png");
const endmark = require("../../assets/image/marker_location_drop.png");
const currentlocation = require("../../assets/image/current_location.png");
const location = require("../../assets/image/area_icon.png");
const alertpoint = require("../../assets/image/alertpoint.png");


class TripDetails extends Component {

  constructor(props) {
    super(props)

    const { navigation } = this.props;

    this.state = { 
      nodatadialogVisible: false,
      nodatadialogAlertVisible: false,
      dialogVisible: false,
      alertDetaildialogVisible: false,
      alertDetaildialogCurVisible: false,
      alertDetaildialogDetail: {Details:'',Name:''},
      alertIdSelected : '3',
      refreshing: false,
      AlertTripInfo: [{ Name: "",Details: "",longitude: 76.86115626007995,latitude: 28.29452789468645}],AlertTripVisible: false,
      RouteDetails: [],
      mandatoryAlerts:[],
      HistoryData: [],
      showCurrentLocation: false,
      CurrentLocation :{longitude :76.86115626007995,latitude :28.29452789468645 },
      startMarker:{longitude :76.86115626007995,latitude :28.29452789468645 },
      endMarker:{longitude :73.65467000000001,latitude :24.45842 },
      itemId : navigation.getParam('itemId', 'NO-ID'),
      itemIdVec : navigation.getParam('itemIdVec', 'NO-ID'),
      otherParam :navigation.getParam('otherParam', ''),
      loginData :navigation.getParam('loginData', ''),
      tripType :navigation.getParam('tripType', 'closeTrip')
    } 
  }

  componentDidMount(){
    this.mounted = true;
    this.getTripPathFromApi();
  }
  componentWillUnmount () {
    fetchData.abort(8);
    fetchData.abort(9);
    fetchData.abort(10);
    this.mounted = false;
  }

  getTripPathFromApi = () => {
    let TripId = this.state.itemId;
    let loginData = this.state.loginData;
    this.setState({refreshing: true});
    tripPathFromApi({TripId: TripId,loginData: loginData})
        .then((res) => {
        
          if (typeof res.RouteDetails !== 'undefined' && res.RouteDetails.length > 0){
             this.setState({RouteDetails :res.RouteDetails});
             this.setState({mandatoryAlerts :res.mandatoryAlerts});
              this.setState({startMarker :{longitude :res.RouteDetails[0].longitude ,latitude :res.RouteDetails[0].latitude }});
              this.setState({endMarker  :{longitude :res.RouteDetails[res.RouteDetails.length- 1].longitude ,latitude :res.RouteDetails[res.RouteDetails.length- 1].latitude }});
            
              this.fitBottomTwoMarkers();
 
              tripHistoryPathFromApi({otherParam: this.state.otherParam,loginData: loginData})
              .then((resSub) => {
                if (typeof resSub.HistoryData !== 'undefined' && resSub.HistoryData.length > 0){
                  this.setState({HistoryData :resSub.HistoryData});
                  if(this.state.tripType !== 'closeTrip' && Object.keys(resSub.CurrentLocation).length > 0){
                    this.setState({CurrentLocation :resSub.CurrentLocation,showCurrentLocation: true});
                  }
                }
                this.setState({refreshing: false});
              })
              .catch((error) => {
                this.setState({refreshing: false});
              });
          }else{
            this.setState({refreshing: false});
            this.setState({nodatadialogVisible: true});
          } 
         
        })
        .catch((error) => {
          console.log(error);
          this.setState({refreshing: false});
        });

        

  }
  fitBottomTwoMarkers = () => {
   // this.map.fitToSuppliedMarkers(['source','destination'], true);
    this.map.fitToCoordinates([this.state.startMarker,this.state.endMarker], {
      edgePadding: DEFAULT_PADDING,
      animated: true,
    });
  }

  _onAlertListPress = (item) => {
    this.setState({ alertIdSelected: item.alertId });
  }

  _onAlertSubmitPress = () => {
    this.setState({dialogVisible: false});
    this.setState({refreshing: true});

    let TripId = this.state.itemId;
    let loginData = this.state.loginData;
    let AlertId = this.state.alertIdSelected;
    tripAlertPointsFromApi({TripId: TripId,AlertId: AlertId,loginData: loginData})
    .then((resSub) => {
      this.setState({refreshing: false});
      if (typeof resSub.TripInfo !== 'undefined' && resSub.TripInfo.length > 0){
        this.setState({AlertTripVisible: true,AlertTripInfo :resSub.TripInfo});
      }else{
        console.log('resSub.TripInfo');
        this.setState({AlertTripVisible: false});
        this.setState({nodatadialogAlertVisible: true});
      }
    })
    .catch((error) => {
      this.setState({refreshing: false});
    });
  }

  onMapPress = (marker) => {
    this.setState({alertDetaildialogDetail: marker});
    this.setState({alertDetaildialogVisible: true,alertDetaildialogCurVisible:false});
  }

  onMapCurrentLocationPress = () => {
    this.setState({alertDetaildialogCurVisible: true,alertDetaildialogVisible: false});
  }

  render() {
    return (
        <Container>
          <MapView
            ref={ref => { this.map = ref; }}
            provider={PROVIDER_GOOGLE}
            style={{...StyleSheet.absoluteFillObject}}
            zoomEnabled={true}
            showsUserLocation = {false}
            followUserLocation = {false}
            initialRegion={{
              latitude: LATITUDE,
              longitude: LONGITUDE,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }}
            customMapStyle={mapStylejson}
          >
            <Polyline
              coordinates={this.state.RouteDetails}
              strokeColor="#000"
              fillColor="#000"
              strokeWidth={3}
              lineCap="butt" 
              lineJoin="round"
            />
            <Polyline
              coordinates={this.state.HistoryData}
              strokeColor="#203d92"
              fillColor="#203d92"
              strokeWidth={3}
              lineCap="butt" 
              lineJoin="round"
            />
            {!this.state.refreshing && <Marker
                  image={startmark}
                  identifier='source'
                  key='source'
                  coordinate={this.state.startMarker}
                  >
                  {/* <Image source={startmark} style={{ width: 20, height: 25 }} /> */}
              </Marker>
            }
            {this.state.showCurrentLocation && <Marker
                image={currentlocation}
                identifier='CurrentLocation'
                key='CurrentLocation'
                coordinate={this.state.CurrentLocation}
                onPress={(e) => this.onMapCurrentLocationPress()}
                >
                {/* <Image source={currentlocation} style={{ width: 30, height: 30 }} /> */}
            </Marker>
            }
            {!this.state.refreshing && <Marker
                image={endmark}
                identifier='destination'
                key='destination'
                coordinate={this.state.endMarker}
                > 
                {/* <Image source={endmark} style={{ width: 20, height: 25 }} /> */}
            </Marker>
            }
            {this.state.AlertTripVisible &&  this.state.AlertTripInfo.map(marker => (
            <Marker
                image={alertpoint}
                key={marker.Details}
                coordinate={{latitude: marker.latitude,longitude : marker.longitude}}
                onPress={(e) => this.onMapPress(marker)}
                > 
                {/* <ImageBackground source={alertpoint} style={{ width: 30, height: 30 }} /> */}
            </Marker>
            ))} 
          </MapView>
        {/* <View style={{ flex: 1 }}> */}
          <CustomSubHeader  
            getHeaderTextone={'Trip Details'} 
            getHeaderTexttwo={this.state.itemIdVec} 
            isSerIconVisible={false} 
            onPress={() => this.props.navigation.goBack()}  >
          </CustomSubHeader>

          {this.state.refreshing && <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
          }
         
          {this.state.alertDetaildialogVisible && <View style={{backgroundColor: colors.white  , position: 'absolute',bottom:0,right:0,left:0, justifyContent: 'flex-end' }}>
            <TouchableOpacity onPress={() => this.setState({alertDetaildialogVisible: false})} style={{flexDirection: 'row',justifyContent: 'space-between',paddingRight: 15,paddingTop: 15,paddingLeft: 10}} >
                <View style={{backgroundColor: colors.red500, borderRadius: 20, paddingVertical: 6, paddingHorizontal: 12}}>
                <H2 style={{color: colors.white,fontWeight: '200'}} >
                 {this.state.alertDetaildialogDetail.Name}
                </H2>
                </View>
                <View  style={{justifyContent: 'center'}} >
                    <Icon name="md-close" style={{ color: colors.gray600}} />
                </View>
            </TouchableOpacity>
            <H2 style={{marginLeft: 15,marginBottom: 90,marginRight: 15,marginTop: 15}} >{this.state.alertDetaildialogDetail.Details}</H2> 
          </View>
          }

          {this.state.alertDetaildialogCurVisible && <View style={{backgroundColor: colors.white  , position: 'absolute',bottom:0,right:0,left:0, justifyContent: 'flex-end' }}>
            <TouchableOpacity onPress={() => this.setState({alertDetaildialogCurVisible: false})} style={{flexDirection: 'row',justifyContent: 'space-between',paddingRight: 15,paddingTop: 15,paddingLeft: 10}} >
                <View style={{backgroundColor: colors.primaryColour, borderRadius: 20, paddingVertical: 6, paddingHorizontal: 12}}>
                <H2 style={{color: colors.white,fontWeight: '200'}} >
                {this.state.CurrentLocation.State}
                </H2> 
                </View>
                <View  style={{justifyContent: 'center'}} >
                    <Icon name="md-close" style={{ color: colors.gray600}} />
                </View>
            </TouchableOpacity>
            <View style={{  flex: 1, flexDirection: 'column'}}>
                   <View style={{flex: 1, flexDirection: 'row',padding:15,justifyContent: 'space-between'}}>
                    <H2 style={{ fontWeight: "bold"}}>
                      {this.state.itemIdVec}
                    </H2>
                    <H3>
                      {this.state.CurrentLocation.date}
                    </H3>
                    </View> 
                    <View style={{flex: 1, flexDirection: 'row',paddingRight:15,paddingLeft:15}}>
                        <Image source= {location} style={{height:15,width:15}}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3>{this.state.CurrentLocation.Location}</H3> 
                        </View>
                    </View> 
                    <View style={{flex: 1,marginBottom: 50,marginTop:20, flexDirection: 'column',alignItems: 'flex-start',paddingRight:15,paddingLeft:15}}>
                     <H2 style={{fontWeight: "bold",color:colors.red500}}>SPEED</H2>
                     <H2>{this.state.CurrentLocation.Speed}</H2>
                 </View>
              </View>
           </View>
          }
          {!this.props.refreshing && 
           <Fab
              active={this.state.active}
              containerStyle={{ marginTop: 70}}
              style={{ backgroundColor: colors.white }}
              position="topRight"
              onPress={() =>  this.setState({dialogVisible: true})}>  
              <Icon name='warning' style={{color: colors.red900}} />
            </Fab>
          }
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: colors.white }}
            position="bottomRight"
            onPress={() => this.fitBottomTwoMarkers()}>
               <Icon name='md-locate' style={{color: colors.black}} />
          </Fab>
        {/* </View> */} 
          <Dialog 
            visible={this.state.dialogVisible}  
            onTouchOutside={() => this.setState({dialogVisible: false})} >
            <TouchableOpacity   onPress={() => this.setState({dialogVisible: false})} style={{backgroundColor: colors.primaryColour,flexDirection: 'row',justifyContent: 'space-between',padding:10}} >
              <H1 style={{color: colors.white,fontWeight: '200'}} >
                  Select Alert
                </H1> 
                <View  style={{justifyContent: 'center'}} >
                  <Icon name="md-close" style={{ color: colors.white}} />
                </View>
            </TouchableOpacity>
            <FlatList
              data={this.state.mandatoryAlerts}
              extraData={this.state}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => { 
                return (
                <TouchableOpacity style={{flexDirection: 'row',marginTop:20}} onPress={() => this._onAlertListPress(item)} >
                  <CheckBox 
                  onPress={() => this._onAlertListPress(item)}
                  checked={this.state.alertIdSelected === item.alertId ? true : false }
                  /> 
                  <H2 style={{marginLeft: 20}} >{item.alertName}</H2> 
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => String(index)}
              />
              <View style={{marginTop: 20}} >
                <Button full rounded  primary style={{margin :10}}   onPress={() =>  this._onAlertSubmitPress() }   >
                  <Text>SUBMIT</Text>
                </Button>
              </View> 
          </Dialog> 
          <Dialog 
            visible={this.state.nodatadialogVisible} 
            >
            <View>
              <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
                <View  style={{padding:10}}>
                <H2 style={{fontWeight: '100',textAlign: 'center'}} >
                No Records Available
                </H2> 
                </View>
                <Button full rounded  primary style={{margin :10}}    onPress={() => this.props.navigation.goBack()} >
                  <Text>CLOSE</Text>
                </Button>
              </View>
            </View>
          </Dialog>  
          <Dialog 
            visible={this.state.nodatadialogAlertVisible} 
            onTouchOutside={() => this.setState({nodatadialogAlertVisible: false})} >
            <View>
              <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
                <View  style={{padding:10}}>
                <H2 style={{fontWeight: '100',textAlign: 'center'}} >
                No Records Available
                </H2> 
                </View>
                <Button full rounded  primary style={{margin :10}}    onPress={() =>  this.setState({nodatadialogAlertVisible: false})} >
                  <Text>CLOSE</Text>
                </Button>
              </View>
            </View>
          </Dialog>
        </Container>
    );
  }
}

export default TripDetails;
