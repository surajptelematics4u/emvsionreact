import React, { Component } from 'react'
import { Image,View,TouchableOpacity,StatusBar,Text,ListView } from 'react-native';
import { Card,H3,H2,Container,Button } from 'native-base';
import EasyListView from '../../component/list/EasyListView';
import fetchData from '../../component/CancelableFetch';

import  Dialog  from '../../component/dialogs/Dialog';
import styles from "./styles";
import colors from "../../theme/color"
import { getMoreInfoOnTrip } from '../../actions/ApiAction';
import CustomHeader from '../../component/CustomHeader';

const idle = require("../../assets/image/location_icon_blue.png");

const locationstart = require("../../assets/image/location_start.png");
const locationend = require("../../assets/image/location_end.png");
const distanceicon = require("../../assets/image/distance_icon.png");

var ds = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 });

class TripList extends Component {

    static defaultProps = {
    empty: false,
    error: false,
    noMore: true,
    column: 1
  }

  componentDidMount(){
    this.state.mounted = true;
    //this.getDataFromApi();
  }
  componentWillUnmount () {
    fetchData.abort(7);
    this.state.mounted = false;
  }

  constructor(props) {
    super(props)

    const { navigation } = this.props;
    
    this.state = {
      nodatadialogVisible: false,
      mounted:false,
      isComponentNotActive: false,
      tagName : navigation.getParam('tagName', 'NO-ID'),
      tagselected :navigation.getParam('tagselected', ''),
      loginData :navigation.getParam('loginData', ''),
      dataSource: ds.cloneWithRows([]),
      rawData: []
    }
     this.renderListItem = this._renderListItem.bind(this) 
     this.onFetch = this._onFetch.bind(this)
  }

  render() {
  //  const footerHeight = Dimensions.get('screen').height/2;

    return (
      <Container>
         <StatusBar barStyle="light-content" />  
      <CustomHeader  getHeaderText={this.state.tagName} isbackVisible={true} onPress={() => this.props.navigation.goBack()}   />
      <EasyListView
        ref={component => this.listview = component}
        column={1}
        renderItem={this._renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetch}
        emptyContent={'No record found.'}
      />
      {/* <Content >
      {this.state.startprogress && <View style={{width: null,height:footerHeight,justifyContent: 'center',alignItems: 'center'}} >
                <ActivityIndicator size="large" color={colors.primaryColour} />
                </View>
      }
      {this.state.dataSource.getRowCount() !== 0 && <ListView
             style={{flex:1}}
             dataSource={this.state.dataSource}
             renderRow={(item) => this._renderListItem(item)}
            />
      }
      </Content> */}
      <Dialog 
          visible={this.state.nodatadialogVisible} 
          >
          <View>
            <View style={{flexDirection: 'column',justifyContent: 'space-between'}} > 
              <View  style={{padding:10}}>
              <H2 style={{fontWeight: '100',textAlign: 'center'}} >
              No Records Available
              </H2> 
              </View>
              <Button full rounded  primary style={{margin :10}}    onPress={() => this.props.navigation.goBack()} >
                <Text style={{color: colors.white}} >CLOSE</Text>
              </Button>
            </View>
          </View>
          </Dialog>  
    </Container>
    )
  }

  _onListPress = (rowData) => {
    this.setState({isComponentNotActive: true});
    this.props.navigation.navigate('TripDetails', {
      itemId: rowData.TripNo,
      itemIdVec: rowData.VehicleNo,
      otherParam: rowData,
      loginData: this.state.loginData,
      tripType: 'openTrip'
    });
   };

  _renderListItem = (rowData, sectionID, rowID, highlightRow) => {
    let component = null;
    switch(rowData.Speed) {
      default:
        component = <Image  source= {idle} style={styles.listIcon}/>
    }

    return ( 
            <TouchableOpacity id={rowData}  onPress={() => this._onListPress(rowData)} >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                    <Image  source= {idle} style={styles.listIcon}/>
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                            {rowData.LR_NO} 
                        </H2>
                        <H3 key={rowData.VehicleNo}  style={{ fontSize:10}}>
                            {rowData.VehicleNo}
                        </H3>
                    </View>
                    <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3 key={"date"}  >
                            {rowData.EndDate}
                        </H3>
                    </View>
                  </View>
                   <View>
                   <View style={{position: 'absolute',top:0,bottom:0,right:0,left:0,marginLeft:12,marginTop:25,borderLeftColor: colors.verylightgray,borderLeftWidth: 1}} />
                    <View style={[styles.listItemContainer3]}>
                        <Image source= {locationstart}  style={[styles.otherIcon,{marginTop: 10,paddingBottom:10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10,paddingBottom:10}}>
                        <H3  key={rowData.Origin}>{rowData.Origin}</H3>
                        </View>
                    </View>
                  </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {locationend}  style={[styles.otherIcon]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3 key={rowData.Destination}>{rowData.Destination}</H3>
                        </View>
                    </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {distanceicon} style={[styles.otherIcon,{marginTop: 10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10}}>
                        <H3  >{rowData.Distance} kms</H3>
                        </View>
                    </View>  
                  </View>
                </Card>
             </TouchableOpacity> 
    )
  }

  getDataFromApi = (pageNo, success, failure) => {
    getMoreInfoOnTrip({ tripStatus: this.state.tagselected, PageNo:1, loginData : this.state.loginData })
        .then((res) => {
          //if(!this.state.isComponentNotActive){
            if (typeof res.OnTripDetails !== 'undefined' && res.OnTripDetails.length > 0) {
              success(res.OnTripDetails);
            }else{
              this.setState({nodatadialogVisible: true});
            }
          // }else{
          //   failure('loding fail...', null); 
          // } 
        })
        .catch((error) => {
        //  if(this.state.mounted) { this.setState({startprogress :false}); }
          failure('connectionError', null);
        });
  }

  _onFetch(pageNo, success, failure) {
      if (this.props.noMore) {
        if (pageNo === 1) {
          this.getDataFromApi(pageNo, success, failure);
        }
        else {
          success([])
        }
      }
  }

}

export default TripList;
