import React, { Component } from 'react';
import { Image,View,TouchableOpacity,ListView,StatusBar } from 'react-native';
import { Container,Content,Fab,H2,Toast,Card,H3 } from 'native-base';

import CustomHeader from '../../component/CustomHeader';
import SearchBar from '../../component/SearchBar';
import colors from '../../theme/color';
import EasyListView from '../../component/list/EasyListView'

import fetchData from '../../component/CancelableFetch';
import { tripSearchAPI } from '../../actions/ApiAction';
import styles from "./styles";

const idle = require("../../assets/image/location_icon_blue.png");

const locationstart = require("../../assets/image/location_start.png");
const locationend = require("../../assets/image/location_end.png");
const distanceicon = require("../../assets/image/distance_icon.png");
var ds = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 });

class TripSearch extends Component {

  constructor(props) {
    super(props);
    const { navigation } = this.props;
    this.state = {
      mounted:false,
      isComponentNotActive: false,
      clearIconVisible: false,
      textprogressVisible: false,
      loginData : navigation.getParam('loginData', 'NO-ID'),
      tagName : navigation.getParam('tagName', 'Search'),
      textSearch: '',
      PageNo : 0,
      dataSource: ds.cloneWithRows([]),
      rawData: []
    };
  }


  componentDidMount(){
 
  }

  onChangeText = (text) => {
    this.setState({PageNo: 0 ,rawData: []}); 

    if(text.length > 0){
      this.setState({ clearIconVisible : true});
      if(text.length > 2){
        fetchData.abort(1);
        this.gettripSearchAPI(text,0); 
      } 
    }else{
      this.onClearText();
    }
  } 

  onClearText = () => {
    this.setState({ clearIconVisible : false,textSearch: ''});
    this.setState({PageNo: 0,rawData: [],dataSource: ds.cloneWithRows([])}); 
    fetchData.abort(1);
  }

  gettripSearchAPI = (text,page) => {
    let newPage = page + 1;
    let loginData = this.state.loginData;
    this.setState({ textprogressVisible : true,textSearch: text,PageNo: newPage});
    tripSearchAPI({ searchText: text,PageNo: newPage,loginData : loginData })
        .then((res) => {
          this.setState({ textprogressVisible : false});
          
          var newData = res.TripInfo instanceof Array ? res.TripInfo : JSON.parse(res.TripInfo);
          var allData = (this.state.PageNo == 1) ? newData : this.state.rawData.concat(newData);

         // if(!this.state.isComponentNotActive){
            this.setState({ rawData: allData});
            this.setState({dataSource: ds.cloneWithRows(allData)});
       //   }
        })
        .catch((error) => {
          if(this.state.mounted) { this.setState({textprogressVisible :false}); }
          Toast.show({
            text: error+'', 
            type: "danger"
          });
        });
  }

  onEndReached = () => {  
    let text = this.state.textSearch;
    let page = this.state.PageNo;
    this.gettripSearchAPI(text,page); 
  }

  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />  
        <CustomHeader isbackVisible={true}  getHeaderText={'Trip Search'} isSerIconVisible={false}  onPress={() => this.props.navigation.goBack()}  />
        <View style={{height: 50}}>
          <SearchBar
              placeholder="Enter Vehicle or Trip Name"
              autoCapitalize={'characters'}
              returnKeyType={'done'}
              autoCorrect={false}
              maxLength={20}
              onClearTextValue={this.onClearText}
              onChangeTextValue={this.onChangeText}
              clearIconVisible ={this.state.clearIconVisible}
              textprogressVisible ={this.state.textprogressVisible}
              />
        </View>
          {this.state.dataSource.getRowCount() !== 0 && <ListView
             style={{flex:1}}
             dataSource={this.state.dataSource}
             renderRow={(item) => this._renderListItem(item)}
             onEndReached={this.onEndReached}
            />
          }
          {/* {this.state.dataSource.getRowCount() === 0 && <View  style={{flex:1,justifyContent: 'center',alignContent:'center'}}>
            <H2>   </H2>
          </View>
          } */}
      </Container>
    );
  }

  _onListPress = (rowData) => {
    let tripType = 'closeTrip';
    if(rowData.tripType === 'OPEN'){
      tripType = 'openTrip';
    }else{
      tripType = 'closeTrip';
    }

    this.setState({isComponentNotActive: true});

    this.props.navigation.navigate('TripDetails', {
      itemId: rowData.TripID,
      itemIdVec: rowData.VehicleNo,
      otherParam: rowData,
      loginData: this.state.loginData,
      tripType: tripType
    });
   };

  _renderListItem(rowData) {
    let component = null;
    switch(rowData.Speed) {
      default:
        component = <Image  source= {idle} style={styles.listIcon}/>
    }

    return ( 
            <TouchableOpacity id={rowData}  onPress={() => this._onListPress(rowData)} >
              <Card style={{padding : 10,marginRight : 10 ,marginLeft : 10}}>
                <View style={styles.listItemContainer}>
                  <View style={styles.ItemContainer1}>
                    <View>
                    <Image  source= {idle} style={styles.listIcon}/>
                    </View>
                    <View style={{paddingLeft:5,flexDirection:'column'}}>
                       <H2 style={styles.VehicleName}>
                            {rowData.tripNo}
                        </H2>
                        <H3 key={rowData.VehicleNo}  style={{ fontSize:10}}>
                            {rowData.VehicleNo}
                        </H3>
                    </View>
                    <View style={{flex :1,alignItems: 'flex-end',justifyContent:  'flex-start'}}>
                        <H3 key={"date"}  >
                            {rowData.EndDate}
                        </H3>
                    </View>
                  </View>
                   <View>
                   <View style={{position: 'absolute',top:0,bottom:0,right:0,left:0,marginLeft:12,marginTop:25,borderLeftColor: colors.verylightgray,borderLeftWidth: 1}} />
                    <View style={[styles.listItemContainer3]}>
                        <Image source= {locationstart}  style={[styles.otherIcon,{marginTop: 10,paddingBottom:10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10,paddingBottom:10}}>
                        <H3  key={rowData.Source}>{rowData.Source}</H3>
                        </View>
                    </View>
                  </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {locationend}  style={[styles.otherIcon]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap'}}>
                        <H3 key={rowData.Destination}>{rowData.Destination}</H3>
                        </View>
                    </View> 
                    <View style={styles.listItemContainer3}>
                        <Image source= {distanceicon} style={[styles.otherIcon,{marginTop: 10}]}>
                        </Image>
                        <View style={{paddingLeft:5,flex: 1, flexWrap: 'wrap',marginTop: 10}}>
                        <H3  >{rowData.Distance} kms</H3>
                        </View>
                    </View>  
                  </View>
                </Card>
             </TouchableOpacity> 
    )
  }
}

export default TripSearch;
