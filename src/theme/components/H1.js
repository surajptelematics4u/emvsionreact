import variable from "./../variables/platform";
import dimention from "../dimention";
import colors from "../color";

export default (variables = variable) => {
  const h1Theme = {
    color: colors.black,
    fontSize: dimention.h1fontsize,
   // lineHeight: variables.lineHeightH2,
  };

  return h1Theme;
};
