import variable from "./../variables/platform";
import dimention from "../dimention";
import colors from "../color";

export default (variables = variable) => {
  const h2Theme = {
    color: colors.black,
    fontSize: dimention.h2fontsize,
   // lineHeight: variables.lineHeightH2,
  };

  return h2Theme;
};
