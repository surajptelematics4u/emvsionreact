import variable from "./../variables/platform";
import dimention from "../dimention";
import colors from "../color";
export default (variables = variable) => {
  const h3Theme = {
    color: colors.black,
    fontSize: dimention.h3fontsize,
   // lineHeight: variables.lineHeightH3
  };

  return h3Theme;
};
