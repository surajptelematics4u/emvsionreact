export const splash_screen_waitTime = 3000;

//export const BASEURL = 'http://111.93.157.33:8080/TelematicsRESTService/services/ServiceProcess/';
//export const  BASEURL = 'http://api.telematics4u.com/TelematicsRESTService/services/ServiceProcess/'; 
//172.16.1.62 ... 111.93.157.33  ...192.168.1.186

export const BASEURL = 'http://track.dhlsmartrucking.com:7080/TelematicsRESTService/services/ServiceProcess/';
export const BASEURLG = '/TelematicsRESTService/services/ServiceProcess/';
export const RESTPASSWORDURI = 'https://www.telematics4u.in/jsps/passwordRecovery.jsp';

//Storage
export const loginData = 'loginData'; //boolean
export const userName = 'userName'; //string
export const drawerNavigation = 'drawerNavigation'; //array
export const FeatureMenu = 'FeatureMenu'; //array

//navigation
// export const loginScreen = 'Login';
// export const homeScreen = 'Home';
// export const SLADashboardDraw = 'Drawer';

