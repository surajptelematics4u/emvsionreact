
import {StyleSheet,Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;

export const MARKERWIDTH = (width * 0.2);
export const MARKERHEIGHT = (width * 0.2);

export const LATITUDE = 22.062867;
export const LONGITUDE = 80.110483;
export const LATITUDE_DELTA = 60.1522;
export const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export const LATITUDE_DELTASUB =   0.07864195044303443;
export const LONGITUDE_DELTASUB =  0.070142817690068;